package wwt.zouki;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import wwt.zouki.interfaces.EventListener;
import wwt.zouki.interfaces.FragmentListener;

/**
 * Created by Ram Prasad on 7/3/2015.
 */
public class BaseActivity extends AppCompatActivity implements EventListener {
    protected FragmentListener mFragment;
    protected FrameLayout progressLayout;
    private AlertDialog alertDialog;

    protected void initLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void setFragment(BaseFragment fragment) {
        this.mFragment = fragment;
    }

    @Override
    public void navigateToFragment(Fragment fragment, String tag) {
        navigateToFragment(fragment, tag, false);
    }

    @Override
    public void navigateToFragment(Fragment fragment, String tag, boolean clearTop) {
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();

        if (clearTop) {
            getFragmentManager().popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            ft.addToBackStack(tag);
        }

        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @Override
    public void showProgressDialog() {
        if (progressLayout != null) {
            progressLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressDialog() {
        if (progressLayout != null) {
            progressLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showAlert(String message, final DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message.replace("\\n", "\n"));
        builder.setCancelable(false);

        builder.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        alertDialog = null;

                        if (listener != null) {
                            listener.onClick(dialog, which);
                        }
                    }
                });

        if (listener != null) {
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    alertDialog = null;
                }
            });
        }

        alertDialog = builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mFragment != null && !mFragment.isBackPressed()) {
            getSupportFragmentManager().popBackStack();

            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                super.onBackPressed();
            }
        }
    }
}
