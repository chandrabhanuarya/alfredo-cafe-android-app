package wwt.zouki;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import wwt.zouki.events.BusProvider;
import wwt.zouki.interfaces.AlertListener;
import wwt.zouki.interfaces.EventListener;
import wwt.zouki.interfaces.FragmentListener;
import android.view.*;
import android.widget.EditText;

import static com.activeandroid.Cache.getContext;

/**
 *
 * Created by Ram Prasad on 7/3/2015.
 */
public class BaseFragment extends Fragment implements FragmentListener {
    private static final String TAG = "Zouki";
    protected EventListener mActivity;
    protected AlertDialog alertDialog;

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mActivity = (EventListener) activity;
            mActivity.setFragment(this);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mActivity = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //View view= getView();
        TextView titleTxt = (TextView) getActivity().findViewById(R.id.titleTxt);

        if (titleTxt != null) {
            titleTxt.setText(getTitle());
        }
    }

    protected void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(int msgId) {
        showToast(getString(msgId));
    }

    public void showAlertEditText(@NonNull String message , final int alertId,final  AlertListener alertListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Title");
        // I'm using fragment here so I'm using getView() to provide ViewGroup
        // but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.alert_textfield, (ViewGroup) getView(), false);
        // Set up the input
        //final EditText input = (EditText) viewInflated.findViewById(R.id.input);

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
               // m_Text = input.getText().toString();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void showConfirmationAlert(@NonNull String message, final int alertId, final AlertListener alertListener) {
        // if we are already showing a confirmation dialog, don't show another
        if (TextUtils.isEmpty(message) || (alertDialog != null
                && alertDialog.isShowing())) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message.replace("\\n", "\n"));
        builder.setCancelable(false);
        builder.setNegativeButton(" ",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            alertDialog.dismiss();
                            getActivity().finish();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception: ", e);
                        }

                        alertDialog = null;

                        if (alertListener != null) {
                            alertListener.onNegativeButtonClick(alertId);
                        }
                    }
                });
        builder.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            alertDialog.dismiss();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception: ", e);
                        }

                        alertDialog = null;

                        if (alertListener != null) {
                            alertListener.onPositiveButtonClick(alertId);
                        }
                    }
                });

        try {
            alertDialog = builder.show();

            TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);
            messageText.setHeight(200);
        } catch (Exception e) {
            Log.e(TAG, "Exception: ", e);
        }
    }



    protected void showAlertDialog(String message) {
        if (mActivity != null) {
            mActivity.showAlert(message,null);
        }
    }

    @Override
    public boolean isBackPressed() {
        return false;
    }


    public void initLayout() {

    }

    public void showProgressDialog() {
        mActivity.showProgressDialog();
    }

    public void hideProgressDialog() {
        mActivity.hideProgressDialog();
    }

    protected String getTitle() {
        return "Products";
    }
}
