package wwt.zouki;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import wwt.zouki.fragments.CategoryListFragment;
import wwt.zouki.fragments.ManageAccountFragment;
import wwt.zouki.fragments.ManageCartFragment;
import wwt.zouki.fragments.MenuListFragment;
import wwt.zouki.fragments.UserAccountDtlsFragment;
import wwt.zouki.utils.ZoukiCart;
import android.content.Context;
/**
 * Created by Ram Prasad on 11/11/2015.
 */
public class MainActivity extends BaseActivity implements OnClickListener {
    private LinearLayout[] tabList;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";
    public static final String Password = "passwordKey";
    public static final String Email = "emailKey";
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        setContentView(R.layout.activity_home);
        initLayout();
    }

    @Override
    protected void initLayout() {
        super.initLayout();

        progressLayout = (FrameLayout) findViewById(R.id.progressLayout);
        tabList = new LinearLayout[3];

        tabList[0] = (LinearLayout) findViewById(R.id.home);
        tabList[0].setOnClickListener(this);

        tabList[1] = (LinearLayout) findViewById(R.id.cart);
        tabList[1].setOnClickListener(this);

        tabList[2] = (LinearLayout) findViewById(R.id.user);
        tabList[2].setOnClickListener(this);

        tabList[0].performClick();
        initBadges();
    }

    public void initBadges() {
        int count = ZoukiCart.getInstance().getCartList().size();

        TextView badge = (TextView) tabList[1].findViewById(
                R.id.badge_cart);

        badge.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        badge.setText(count + "");
    }

    @Override
    public void onClick(View v) {
        for (LinearLayout tab : tabList) {
            tab.setBackgroundDrawable(getResources().getDrawable(tab.getId() == v.getId()
                    ? R.drawable.tab_selected : R.drawable.tab_unselected));

            if (tab.getId() == v.getId()) {
                onTabSelected(v);
            }
        }
    }

    private void onTabSelected(View v) {
        Fragment fragment = null;

        switch (v.getId()) {
            case R.id.home:
                fragment = new CategoryListFragment();
                break;
            case R.id.cart:
                fragment = new ManageCartFragment();
                break;
            case R.id.user:
                fragment = ZoukiCart.getInstance().getUserDtls() != null ? new UserAccountDtlsFragment()
                        : new ManageAccountFragment();
                break;

            default:
        }

        getSupportFragmentManager().popBackStack(null, FragmentManager
                .POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();

        fragment.setArguments(getIntent().getExtras());
        ft.addToBackStack(fragment.getTag());

        ft.add(R.id.container, fragment).commit();
    }

}
