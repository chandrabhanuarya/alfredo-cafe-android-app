package wwt.zouki;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.app.*;
import android.content.*;
import android.widget.TextView;

import wwt.zouki.fragments.CategoryListFragment;
import wwt.zouki.fragments.ManageAccountFragment;
import wwt.zouki.fragments.ManageCartFragment;
import wwt.zouki.fragments.MenuListFragment;
import wwt.zouki.fragments.UserAccountDtlsFragment;
import wwt.zouki.utils.ZoukiCart;

public class MenuListActivity extends MainActivity   {
    private LinearLayout[] tabList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu_layout);
        initLayout();
    }

    protected void initLayout() {

        progressLayout = (FrameLayout) findViewById(R.id.progressLayout);

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();

        ft.add(R.id.container, new MenuListFragment()).commit();
    }

    public void initBadges() {
        int count = ZoukiCart.getInstance().getCartList().size();
        if(count != 0) {

            String cateogry = ZoukiCart.getInstance().getServiceTypeInCart().getTag();

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("You Have Existing Cart Items in " + cateogry.toUpperCase()+ " category!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }



    }



}

