package wwt.zouki;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.TextView;

/**
 * Created by Ram Prasad on 7/3/2015.
 */
public class SplashActivity extends BaseActivity implements AnimationListener {
    Animation animFadein,animBounce, animSlideDown, animRotate, animBlink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);

        animBounce = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);

        animRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);

        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);

        animBlink = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);

        animFadein.setAnimationListener(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager
                .LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);



        TextView welcome = (TextView) findViewById(R.id.welcome);

        TextView alfredo = (TextView) findViewById(R.id.alfredo_title);

        TextView smartapp = (TextView) findViewById(R.id.smartapp);

        // start the animation
        welcome.startAnimation(animFadein);
        alfredo.startAnimation(animBlink);
        alfredo.startAnimation(animFadein);
        smartapp.startAnimation(animBounce);


        initLayout();
    }

    protected void initLayout() {
        new SplashTask().execute();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    private class SplashTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            startActivity(new Intent(SplashActivity.this, MenuListActivity.class));
        }
    }
}
