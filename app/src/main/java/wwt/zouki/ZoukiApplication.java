package wwt.zouki;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

import wwt.zouki.api.ZoukiApi;
import wwt.zouki.events.BusProvider;
import wwt.zouki.utils.PreferenceManager;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class ZoukiApplication extends Application {
    private static final String POP_UP_SHOWN = "isPopupShown";

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this, true);

        // listen for global events
        BusProvider.getInstance().register(this);

        // make sure our service gets instantiated when the applications starts,
        // because this is kind of an expensive operation...
        // NOTE: this will also register all of the events in ZoukiApi with the event bus!
        ZoukiApi.getService();
    }

    public boolean isPlacePopupShown() {
        return new PreferenceManager(this).getBooleanPref(POP_UP_SHOWN);
    }

    public void setPlacePopupShown(boolean placePopupShown) {
        new PreferenceManager(this).setBooleanPref(POP_UP_SHOWN, placePopupShown);
    }
}
