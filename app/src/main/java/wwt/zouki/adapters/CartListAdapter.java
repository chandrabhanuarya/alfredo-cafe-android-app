package wwt.zouki.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import wwt.zouki.BaseActivity;
import wwt.zouki.R;
import wwt.zouki.fragments.RoundedTransformation;
import wwt.zouki.interfaces.CartEventListener;
import wwt.zouki.model.Cart;
import wwt.zouki.utils.ApplicationUtils;
import wwt.zouki.utils.ZoukiCart;

/**
 * Created by Ram Prasad on 7/9/2015.
 */
public class CartListAdapter extends BaseAdapter {
    private CartEventListener listener;
    private LayoutInflater inflater;
    private List<Cart> cartList;
    private boolean isCatering;

    public CartListAdapter(Context context, List<Cart> cartList, CartEventListener listener) {
        this.inflater = LayoutInflater.from(context);
        isCatering = ZoukiCart.getInstance().isOnCatering();

        this.listener = listener;
        this.cartList = cartList;
    }

    public void updateCartList(List<Cart> cartList) {
        this.cartList = cartList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return cartList.size();
    }

    @Override
    public Object getItem(int position) {
        return cartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getConvertView();
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        updateView(position, holder);

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        ImageView delete;
        TextView name;

        TextView quantity;
        TextView cost;
        TextView increment;
        TextView decrement;
    }

    private View getConvertView() {
        View view = inflater.inflate(R.layout.item_cart_details, null);
        ViewHolder holder = new ViewHolder();

        holder.imageView = (ImageView) view.findViewById(
                R.id.img_product);

        holder.name = (TextView) view.findViewById(R.id.name);
        holder.cost = (TextView) view.findViewById(R.id.cost);

        holder.quantity = (TextView) view.findViewById(R.id.quantity);
        holder.delete = (ImageView) view.findViewById(R.id.delete);

        holder.increment = (TextView) view.findViewById(R.id.increment);
        holder.decrement = (TextView) view.findViewById(R.id.decrement);

        if (isCart()) {
            holder.delete.setVisibility(View.VISIBLE);
        }

        view.setTag(holder);
        return view;
    }

    private void updateView(int position, ViewHolder holder) {
        Cart cartItem = cartList.get(position);

        holder.name.setText(cartItem.name);
        holder.cost.setText("$" + ApplicationUtils.roundOffValue(cartItem.total));

        if (isCart()) {
            holder.delete.setOnClickListener(new DeleteCartItem(position));

            holder.decrement.setOnClickListener(new CustomizeOrderListener(
                    false, position));
            holder.increment.setOnClickListener(new CustomizeOrderListener(
                    true, position));
        }

        Picasso.with(inflater.getContext()).load(cartItem.imageURL).error(
                R.mipmap.icon_small).transform(new RoundedTransformation(100, 0)).into(holder.imageView);

        holder.quantity.setText(cartItem.quantity + "");
    }

    protected boolean isCart() {
        return true;
    }

    private class DeleteCartItem implements View.OnClickListener {
        private int position;

        public DeleteCartItem(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Context context = inflater.getContext();

            ((BaseActivity) context).showAlert(context.getString(R.string.delete_cart_item),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (listener != null) {
                                listener.deleteItem(position);
                                listener.onCartUpdated();
                            }
                        }
                    });
        }
    }

    private class CustomizeOrderListener implements View.OnClickListener {
        private boolean increment;
        private int position;

        public CustomizeOrderListener(boolean increment, int position) {
            this.increment = increment;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Cart item = cartList.get(position);

            if (increment) {
                item.quantity++;
            } else {
                if (item.quantity > (isCatering ? 10 : 1)) {
                    item.quantity--;
                }
            }

            item.total = item.price * item.quantity;
            item.save();

            notifyDataSetChanged();
            listener.onCartUpdated();
        }
    }

}
