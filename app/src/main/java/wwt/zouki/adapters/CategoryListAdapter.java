package wwt.zouki.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import wwt.zouki.R;
import wwt.zouki.api.data.Category;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class CategoryListAdapter extends BaseAdapter {
    private List<Category> categories;
    private LayoutInflater inflater;

    public CategoryListAdapter(Context context, List<Category> categories) {
        this.categories = categories;
        this.inflater = LayoutInflater.from(context);
    }

    public void updateCategories(List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_category, null);
        }

        ((TextView) convertView.findViewById(R.id.categoryName)).setText(
                categories.get(position).name);


        return convertView;
    }
}
