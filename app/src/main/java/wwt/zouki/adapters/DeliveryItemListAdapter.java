package wwt.zouki.adapters;

import android.content.Context;

import java.util.List;

import wwt.zouki.interfaces.CartEventListener;
import wwt.zouki.model.Cart;

/**
 * Created by Ram Prasad on 9/18/2015.
 */
public class DeliveryItemListAdapter extends CartListAdapter {

    public DeliveryItemListAdapter(Context context, List<Cart> cartList, CartEventListener listener) {
        super(context, cartList, listener);
    }

    @Override
    protected boolean isCart() {
        return false;
    }
}