package wwt.zouki.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import wwt.zouki.api.data.Option;
import wwt.zouki.interfaces.AddOnEventListener;
import wwt.zouki.views.OptionsCardView;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class OptionListAdapter extends BaseAdapter {
    private AddOnEventListener mListener;
    private List<Option> optionList;
    private Context context;

    public OptionListAdapter(Context context, List<Option> options, AddOnEventListener listener) {
        this.optionList = options;
        this.context = context;

        this.mListener = listener;
    }

    public void updateOptions(List<Option> options) {
        this.optionList = options;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return optionList.size();
    }

    @Override
    public Object getItem(int position) {
        return optionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OptionsCardView cardView = new OptionsCardView(context);
        cardView.buildCardView(optionList.get(position), mListener);

        return cardView;
    }

}
