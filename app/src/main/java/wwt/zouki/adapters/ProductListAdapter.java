package wwt.zouki.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import wwt.zouki.fragments.RoundedTransformation;

import java.util.ArrayList;
import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.data.Product;
import wwt.zouki.fragments.CustomizeItemFragment_backup;
import wwt.zouki.interfaces.AddOnEventListener;
import wwt.zouki.interfaces.AlertListener;
import wwt.zouki.model.Cart;
import wwt.zouki.model.OptionItem;
import wwt.zouki.utils.ApplicationUtils;
import wwt.zouki.utils.ZoukiCart;
import wwt.zouki.views.OptionsListView;

import static android.app.PendingIntent.getActivity;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class ProductListAdapter extends BaseAdapter implements AddOnEventListener, AlertListener {
    private List <Product> productList;
    private LayoutInflater inflater;
    private Context context;
    private boolean isSandwich;
    private boolean isCatering;
    private boolean isBeverages;
    private boolean isCafe;
    private Cart item;
    private boolean isFingerFood;
    private TextView cost;
    public BaseFragment baseFragment;
    private int MIN_HEAD_COUNT;
    public boolean isBurger;


    private OptionsListView optionList;
    private WebView description;
    private TextView quantity;
    private TextView help_text;
    private ImageView display;
    private TextView name;
    private String descTxt;
    private ScrollView burger_desp;
    private TextView textView;

    public ProductListAdapter(Context context, List<Product> products, boolean isSandwich, boolean isBeverages, boolean isFingerFood) {
        this.isCatering = ZoukiCart.getInstance().isOnCatering();
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.isCafe = ZoukiCart.getInstance().isOnCafe();
        this.isFingerFood =  isFingerFood;

        Log.d("isFingerFood", String.valueOf(isFingerFood));

        //this.isBeverages = ZoukiConstants.CATEGORY.equalsIgnoreCase();
        this.productList = products;
        this.isSandwich = isSandwich;
        this.isBeverages = isBeverages;
        item = new Cart();
        item.serviceType = ZoukiCart.getInstance()
                .getServiceType();
    }

    public void updateProductList(List<Product> products) {
        this.productList = products;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getConvertView();
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        updateView(position, holder);

        return convertView;
    }

    @Override
    public void updatePrice(OptionItem option, boolean isChecked) {
        try {
            Float addedItem = Float.parseFloat(option.price.replace("$", ""));
//          item.price = item.price + (isChecked ? +addedItem : -addedItem);

            if (isChecked) {
                item.price = item.price + addedItem;
            } else {
                item.price = item.price - addedItem;
            }

            item.total = item.price * item.quantity;
            cost.setText("$" + ApplicationUtils.roundOffValue(item.total));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } finally {
            if (item.options == null) item.options = new ArrayList<>();
            item.options.add(option);

            //TODO: Need to clean it off once conformed
            item.optionDesc = item.optionDesc + (!item.optionDesc.equals("") ? ", "
                    : "") + option.value;
        }
    }

    @Override
    public void updatePrice(OptionItem option, String cur, String prev) {

    }

    @Override
    public void updateComment(OptionItem comment) {

    }

    @Override
    public void updateDateTime(int id, OptionItem dateTime) {

    }

    @Override
    public void onPositiveButtonClick(int alertId) {

    }

    @Override
    public void onNegativeButtonClick(int alertId) {

    }

    private class ViewHolder {
        LinearLayout qtyLayout;
        ImageView thumbnail;
        TextView name;
        TextView cost;

        TextView quantity;
        TextView increment;
        TextView decrement;
        TextView minQtyLbl;
        TextView add_to_cart;
        TextView people;
    }

    private View getConvertView() {
        View view = inflater.inflate(R.layout.item_product, null);
        ViewHolder holder = new ViewHolder();


        holder.name = (TextView) view.findViewById(R.id.name);
        holder.cost = (TextView) view.findViewById(R.id.cost);
        holder.add_to_cart = (TextView) view.findViewById(R.id.add_to_cart);


        holder.qtyLayout = (LinearLayout) view.findViewById(R.id.qntyLayout);
        holder.minQtyLbl = (TextView) view.findViewById(R.id.label_min_qty);


        if (isCatering) {
            holder.qtyLayout.setVisibility(View.VISIBLE);
            //holder.minQtyLbl.setVisibility(View.VISIBLE);
        }
        if(isSandwich){
                Log.d("Sandwich", String.valueOf(isSandwich));
            holder.add_to_cart.setVisibility(View.GONE);
        }

        if (isCafe){
            holder.qtyLayout.setVisibility(View.GONE);
            holder.add_to_cart.setVisibility(View.GONE);

        }
        if(isBeverages){
            Log.d("Beverages check", String.valueOf(isBeverages));
           // holder.qtyLayout.setVisibility(View.VISIBLE);
           // holder.add_to_cart.setVisibility(View.VISIBLE);
        }
        if(isFingerFood){
            holder.cost.setText("Click");
        }

        holder.thumbnail = (ImageView) view.findViewById(R.id.img_product);
        holder.quantity  = (TextView) view.findViewById(R.id.quantity);

        holder.increment = (TextView) view.findViewById(R.id.increment);
        holder.decrement = (TextView) view.findViewById(R.id.decrement);

        view.setTag(holder);
        return view;
    }

    private void updateView(int position, ViewHolder holder) {
        Product product = productList.get(position);

       if(product.thumbnail!= null){
           Picasso.with(context).load(ApplicationUtils.getWellFormedURL(product.thumbnail)).transform(new RoundedTransformation(100, 0))
                   .into(holder.thumbnail);
       }


        holder.cost.setText((isSandwich ? "Starting Price: " : "") + product.price);

        holder.name.setText(product.name);

        if (isCatering) {
            holder.quantity.setText(String.valueOf(product.quantity));
            holder.qtyLayout.setVisibility(View.VISIBLE);

            holder.decrement.setOnClickListener(new CustomizeOrderListener(
                    false, position));
            holder.increment.setOnClickListener(new CustomizeOrderListener(
                    true, position));

        }
        if(isSandwich){

            Fragment fragment = new CustomizeItemFragment_backup();

            holder.decrement.setOnClickListener(new CustomizeOrderListener(
                    false, position));
            holder.increment.setOnClickListener(new CustomizeOrderListener(
                    true, position));
        }
        if(isBeverages){
       /*     holder.quantity.setText(String.valueOf((product.quantity)-9));
            holder.add_to_cart.setVisibility(View.VISIBLE);
            holder.add_to_cart.setOnClickListener(new addtocart());

            holder.decrement.setOnClickListener(new CustomizeOrderBeverages(
                    false, position));
            holder.increment.setOnClickListener(new CustomizeOrderBeverages(
                    true, position));
*/
        }
    }


    private  class addtocart implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if (!optionList.isMandatoryItemsMissed()) {
                new AddItemAsyncTask(item).execute();
            } else {
                baseFragment.showConfirmationAlert(baseFragment.getString(R.string.lbl_required), 0, ProductListAdapter.this);
            }
        }

    }


    private class AddItemAsyncTask extends AsyncTask {
        private Cart cart;

        public AddItemAsyncTask(Cart cart) {
            this.cart = cart;
        }

        @Override
        protected void onPreExecute() {
            baseFragment.showProgressDialog();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            cart.save();

           if (cart.options != null) {
                for (OptionItem item : cart.options) {
                    item.cart = cart;
                    item.save();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            baseFragment.hideProgressDialog();

        //    Toast.makeText(getActivity(), baseFragment.getString(), Toast.LENGTH_SHORT).show();
        //    ((MainActivity) getActivity()).initBadges();

        //    getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private class CustomizeOrderBeverages implements View.OnClickListener {
        private boolean increment;
        private int position;

        public CustomizeOrderBeverages(boolean increment, int position) {
            this.increment = increment;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Product item = productList.get(position);

            if (increment) {
                item.quantity++;
            } else {
                if (item.quantity > 1) {
                    item.quantity--;
                }
            }

            notifyDataSetChanged();
        }
    }




    private class CustomizeOrderListener implements View.OnClickListener {
        private boolean increment;
        private int position;

        public CustomizeOrderListener(boolean increment, int position) {
            this.increment = increment;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Product item = productList.get(position);

            if (increment) {
                item.quantity++;
            } else {
                if (item.quantity > 10) {
                    item.quantity--;
                }
            }

            notifyDataSetChanged();
        }
    }


}
