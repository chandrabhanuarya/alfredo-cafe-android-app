package wwt.zouki.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import wwt.zouki.model.Cart;
import wwt.zouki.views.ServiceItemView;

/**
 * Created by Ram Prasad on 10/24/2015.
 */
public class ServiceListAdapter extends BaseAdapter {
    private List<List<Cart>> services;
    private Context mContext;

    public ServiceListAdapter(Context context, List<List<Cart>> services) {
        this.mContext = context;
        this.services = services;
    }

    public void updateServiceList(List<List<Cart>> services) {
        this.services = services;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return services.size();
    }

    @Override
    public Object getItem(int position) {
        return services.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ServiceItemView serviceItemView = new ServiceItemView(mContext);
        serviceItemView.buildServiceItemList(services.get(position));

        return serviceItemView;
    }
}
