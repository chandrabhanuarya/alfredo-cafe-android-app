package wwt.zouki.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 6/10/2015.
 */
public final class ApiError {
    @SerializedName("message") public int message;
    @SerializedName("error") public boolean error;

    public ApiError(boolean error, int message) {
        this.message = message;
        this.error = error;
    }
}
