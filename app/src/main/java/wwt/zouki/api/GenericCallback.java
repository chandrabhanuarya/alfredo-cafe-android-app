package wwt.zouki.api;

import android.util.Log;

import retrofit.Callback;
import retrofit.RetrofitError;
import wwt.zouki.R;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;

/**
 * Created by Ram Prasad on 6/10/2015.
 */
public abstract class GenericCallback<T> implements Callback<T> {
    private static final String TAG = GenericCallback.class.getSimpleName();

    @Override
    public void failure(RetrofitError error) {
        Log.d(TAG, "");

        BusProvider.getInstance().post(new ApiResponseEvent.Error().setTag(new ApiError(false, R.string.error_message)));
        error.printStackTrace();
    }
}
