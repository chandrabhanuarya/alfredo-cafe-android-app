package wwt.zouki.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Type;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import wwt.zouki.BuildConfig;
import wwt.zouki.R;
import wwt.zouki.api.data.CategoryResponse;
import wwt.zouki.api.data.CompanyBill;
import wwt.zouki.api.data.CostCenter;
import wwt.zouki.api.data.GenericResponse;
import wwt.zouki.api.data.LoginResponse;
import wwt.zouki.api.data.ProductInDetailResponse;
import wwt.zouki.api.data.ProductResponse;
import wwt.zouki.api.data.SmartCard;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.events.CategoryRequestEvent;
import wwt.zouki.events.LoginRequestEvent;
import wwt.zouki.events.ProductInDetailRequestEvent;
import wwt.zouki.events.ProductRequestEvent;
import wwt.zouki.events.SubCategoryRequestEvent;
import wwt.zouki.model.Cart;

/**
 * Created by Ram Prasad on 6/9/2015.
 */
public final class ZoukiApi {
    public static final String SERVER_URL = "http://alfredocafe.com.au/index.php?route=feed/webapi";
   //public static final String SERVER_URL = "http://zoukionline.com.au/index.php?route=feed/webapi";
    private static Bus sBus;

    private static ZoukiApi sInstance;

    private ZoukiService mService;

    public static ZoukiService getService() {
        if (sInstance == null) {
            sInstance = new ZoukiApi();
            getEventBus().register(sInstance);
        }

        return sInstance.mService;
    }

    private ZoukiApi() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setConverter(new GsonConverter(getGson()))
                .setEndpoint(SERVER_URL);

        // add full request/response logs if we are a debug build
        if (BuildConfig.DEBUG) {
            builder.setLogLevel(RestAdapter.LogLevel.FULL)
                    .setLog(new AndroidLog("RetrofitApi"));
        }

        mService = builder.build().create(ZoukiService.class);
    }

    private Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Cart.class, new CartSerializer())
                .create();
    }

    private class CartSerializer implements JsonSerializer<Cart> {

        @Override
        public JsonElement serialize(Cart src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            object.addProperty("delivery_date", src.delivery_date != null
                    ? src.delivery_date.toString() : "");

            object.addProperty("category", src.category);
            object.addProperty("model", src.model);
            object.addProperty("option_desc", src.optionDesc);
            object.addProperty("total", String.valueOf(src.total));
            object.addProperty("product_id", String.valueOf(src.productId));
            object.addProperty("name", src.name);
            object.addProperty("quantity", String.valueOf(src.quantity));
            object.addProperty("price", String.valueOf(src.price));
            object.addProperty("type", src.type);

            object.addProperty("option", new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                    .create().toJson(src.options));

            return object;
        }
    }

    /**
     * Allow the event bus to be set (think mocks for UTs); At least until we add DI...
     *
     * @param bus Instance of the event bus
     */
    public static void setEventBus(Bus bus) {
        sBus = bus;
    }

    /**
     * Gets the event bus
     *
     * @return An sInstance of the event bus
     */
    private static Bus getEventBus() {
        if (sBus == null) {
            sBus = BusProvider.getInstance();
        }

        return sBus;
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void fetchCategories(CategoryRequestEvent event) {
        getService().getCategoryResponse(event.name,
                new GenericCallback<CategoryResponse>() {

                    @Override
                    public void success(CategoryResponse categoryResponse, Response response) {
                        getEventBus().post(new ApiResponseEvent.Categories().setTag(
                                categoryResponse.categories));
                    }
                });
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void fetchSubCategories(SubCategoryRequestEvent event) {
        getService().getSubCategoryResponse(event.id,
                new GenericCallback<CategoryResponse>() {

                    @Override
                    public void success(CategoryResponse categoryResponse, Response response) {
                        getEventBus().post(new ApiResponseEvent.Categories().setTag(
                                categoryResponse.categories));
                    }
                });
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void fetchProducts(ProductRequestEvent event) {
        getService().getProductResponse(event.id,
                new GenericCallback<ProductResponse>() {

                    @Override
                    public void success(ProductResponse productResponse, Response response) {
                        getEventBus().post(new ApiResponseEvent.Products().setTag(
                                productResponse.products));
                    }
                });
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void fetchProductDetails(ProductInDetailRequestEvent event) {
        getService().getProductInDetailResponse(event.itemId,
                new GenericCallback<ProductInDetailResponse>() {

                    @Override
                    public void success(ProductInDetailResponse productDtlsResponse, Response response) {
                        getEventBus().post(new ApiResponseEvent.ProductInDetail().setTag(
                                productDtlsResponse.productDtls));
                    }
                });
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void validateLoginDtls(LoginRequestEvent event) {
        getService().getLoginResponse(event.username, event.password,
                new GenericCallback<LoginResponse>() {

                    @Override
                    public void success(LoginResponse loginResponse, Response response) {
                        if (loginResponse.success) {
                            getEventBus().post(new ApiResponseEvent.UserDetails().setTag(
                                    loginResponse.userDtls));
                        } else {
                            getEventBus().post(new ApiResponseEvent.Error().setTag(
                                    new ApiError(loginResponse.success, R.string.error_message)));
                        }
                    }
                });
    }

    @Subscribe
    public void validateSmartCard(SmartCard event) {
        getService().checkSmartCard(event.email, event.pinNumber,
                new GenericCallback<GenericResponse>() {

                    @Override
                    public void success(GenericResponse result, Response response) {
                        getEventBus().post(new ApiResponseEvent.Success());
                    }
                });

    }

    @Subscribe
    public void checkCostCenter(CostCenter event) {
        getService().checkCostCenter(event.cardNumber, event.pin,
                new GenericCallback<GenericResponse>() {

                    @Override
                    public void success(GenericResponse result, Response response) {
                        getEventBus().post(new ApiResponseEvent.Success());
                    }
                });

    }

    @Subscribe
    public void billToCompany(CompanyBill event) {
        getService().postBillToCompany(event.companyName, event.streetNo, event.streetName, event.suburb, event.postCode,
                event.clientName, event.clientEmail, event.title, event.department, event.fax, event.orderId,
                new GenericCallback<GenericResponse>() {

                    @Override
                    public void success(GenericResponse result, Response response) {
                        getEventBus().post(new ApiResponseEvent.Success());
                    }
                });

    }

//    @Subscribe
//    public void billToCompany(CompanyBill event) {
//        getService().postBillToCompany(event.orderId, event.asQueryMap(),
//                new GenericCallback<GenericResponse>() {
//
//                    @Override
//                    public void success(GenericResponse result, Response response) {
//                        getEventBus().post(new ApiResponseEvent.Success());
//                    }
//                });
//
//    }

}
