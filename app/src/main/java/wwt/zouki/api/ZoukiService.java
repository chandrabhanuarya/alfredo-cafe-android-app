package wwt.zouki.api;

import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import wwt.zouki.api.data.CategoryResponse;
import wwt.zouki.api.data.GenericResponse;
import wwt.zouki.api.data.LoginResponse;
import wwt.zouki.api.data.ProductInDetailResponse;
import wwt.zouki.api.data.ProductResponse;
import wwt.zouki.model.Cart;

/**
 * Created by Ram Prasad on 6/10/2015.
 */
public interface ZoukiService {

    @GET("/categories&name={type}")
    void getCategoryResponse(
            @Path("type") String name,
            Callback<CategoryResponse> callback
    );

    @GET("/categories&subcategory={id}")
    void getSubCategoryResponse(
            @Path("id") int categoryId,
            Callback<CategoryResponse> callback
    );

    @GET("/products&category={id}")
    void getProductResponse(
            @Path("id") int categoryId,
            Callback<ProductResponse> callback
    );

    @GET("/product&id={id}")
    void getProductInDetailResponse(
            @Path("id") int categoryId,
            Callback<ProductInDetailResponse> callback
    );

    @GET("/userlogin&email={email}&password={password}")
    void getLoginResponse(
            @Path("email") String email,
            @Path("password") String password,
            Callback<LoginResponse> callback
    );

    @FormUrlEncoded
    @POST("/checksmartcard")
    void checkSmartCard(
            @Field("email") String email,
            @Field("pin_number") String pinNumber,
            Callback<GenericResponse> callback
    );

    @FormUrlEncoded
    @POST("/checkcostcenter")
    void checkCostCenter(
            @Field("center_number") String cardNo,
            @Field("pin_number") String pinNumber,
            Callback<GenericResponse> callback
    );

    @FormUrlEncoded
    @POST("/billtocompany")
    void postBillToCompany(
            @Field("company_name") String companyName,
            @Field("street_number") String streetNo,
            @Field("street_name") String streetName,
            @Field("suburb") String suburb,
            @Field("post_code") String postCode,
            @Field("client_name") String clientName,
            @Field("client_email") String clientEmail,
            @Field("title") String title,
            @Field("department") String department,
            @Field("fax") String fax,
            @Field("order_id") int orderId,
            Callback<GenericResponse> response
    );

    @FormUrlEncoded
    @POST("/getOrderDetails")
    void postOrderDetails(@FieldMap Map<String, String> order, @Field("cartproducts") List<Cart> cart,
                           Callback<JsonObject> callback);

    @FormUrlEncoded
    @POST("/orderDetails")
    void postOrderDetails2(
                           @Field("jsonParam") JSONArray jsonParam ,
                           Callback<JsonObject> callback);

     @FormUrlEncoded
    @POST("/registration")
    void postRegistration(@Field("name") String name, @Field("email") String email,
                          @Field("password") String password, @Field("phone") String phone,Callback<JsonObject> Callback);

    @FormUrlEncoded
    @POST("/forgotpassword")
    void postForgotPassword(@Field("email") String email, Callback<JsonObject> Callback);


     @FormUrlEncoded
    @POST("/userlogin")
    void postUserLogin(@Field("email") String email, @Field("password") String password, Callback <JsonObject> Callback);

    @FormUrlEncoded
    @POST("/promocode")
    void postPromoCode(@Field("coupon") String coupon, @Field("total_price") String total_price, Callback <JsonObject> Callback );

}











