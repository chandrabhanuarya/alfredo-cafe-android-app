package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class Category {
    @SerializedName("category_id")
    public int categoryId;

    @SerializedName("parent_id")
    public int parentId;

    @SerializedName("name")
    public String name;

    @SerializedName("image")
    public String imageURL;

    @SerializedName("type")
    public String type;

    @SerializedName("operating_hours")
    public String operatingHrs;
}
