package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class CategoryResponse {
    @SerializedName("success")
    public boolean isSuccess;

    @SerializedName("delivery_times")
    public DeliveryTimings deliveryTimings;

    @SerializedName("categories")
    public List<Category> categories;
}
