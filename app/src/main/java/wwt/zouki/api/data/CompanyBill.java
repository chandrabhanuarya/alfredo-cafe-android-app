package wwt.zouki.api.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Ram Prasad on 8/22/2015.
 */
public class CompanyBill implements Parcelable {
    public String companyName;
    public String streetNo;
    public String streetName;
    public String suburb;
    public String postCode;

    public String clientName;
    public String clientEmail;
    public String title;
    public String department;
    public String clientPhone;
    public String fax;
    public int orderId = 345;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.companyName);
        dest.writeString(this.streetNo);
        dest.writeString(this.streetName);
        dest.writeString(this.suburb);
        dest.writeString(this.postCode);
    }

    public CompanyBill() {
    }

    private CompanyBill(Parcel in) {
        this.companyName = in.readString();
        this.streetNo = in.readString();
        this.streetName = in.readString();
        this.suburb = in.readString();
        this.postCode = in.readString();
    }

    public static final Parcelable.Creator<CompanyBill> CREATOR = new Parcelable.Creator<CompanyBill>() {
        public CompanyBill createFromParcel(Parcel source) {
            return new CompanyBill(source);
        }

        public CompanyBill[] newArray(int size) {
            return new CompanyBill[size];
        }
    };

    /**
     * Formats the class properties for use with a {@link retrofit.http.QueryMap}
     * @return Returns the parameters as a {@link java.util.Map}
     */
    public Map<String, String> asQueryMap() {
        Map<String, String> options = new LinkedHashMap<>();
        if (!companyName.equals(""))
            options.put("company_name", companyName);

        if (!companyName.equals(""))
            options.put("street_number", streetNo);

        if (!companyName.equals(""))
            options.put("street_name", streetName);

        if (!companyName.equals(""))
            options.put("suburb", suburb);

        if (!companyName.equals(""))
            options.put("post_code", postCode);

        if (!companyName.equals(""))
            options.put("client_name", clientName);

        if (!companyName.equals(""))
            options.put("client_email", clientEmail);

        if (!companyName.equals(""))
            options.put("title", title);

        if (!companyName.equals(""))
            options.put("department", department);

//        if (!companyName.equals(""))
//            options.put("post_code", clientPhone);

        if (!companyName.equals(""))
            options.put("fax", fax);

        if (!companyName.equals(""))
            options.put("order_id", "345");

        return options;
    }
}
