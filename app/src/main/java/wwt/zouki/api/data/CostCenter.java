package wwt.zouki.api.data;

/**
 * Created by Ram Prasad on 8/21/2015.
 */
public class CostCenter {
    public String cardNumber;
    public String pin;

    public CostCenter(String cardNumber, String pin) {
        this.cardNumber = cardNumber;
        this.pin = pin;
    }
}
