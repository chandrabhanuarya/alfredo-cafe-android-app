package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class DeliveryTimings {
    @SerializedName("pickup")
    public String pickUp;

    @SerializedName("delivery")
    public String delivery;
}
