package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/10/2015.
 */
public class LoginResponse {
    @SerializedName("success")
    public boolean success;

    @SerializedName("user_details")
    public User userDtls;
}
