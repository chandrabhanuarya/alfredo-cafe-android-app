package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class Option {
    @SerializedName("product_option_id")
    public int productOptionId;

    @SerializedName("option_id")
    public int optionId;

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @SerializedName("option_value")
    public List<OptionValue> optionValues;

    @SerializedName("required")
    public int required;
}
