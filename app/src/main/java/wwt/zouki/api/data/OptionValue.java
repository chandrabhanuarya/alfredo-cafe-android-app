package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/8/2015.
 */
public class OptionValue {

    @SerializedName("product_option_value_id")
    public int productOptionValueId;

    @SerializedName("option_value_id")
    public int optionValueId;

    @SerializedName("name")
    public String name;

    @SerializedName("image")
    public String imageURL;

    @SerializedName("price")
    public String price;

    @SerializedName("price_prefix")
    public String pricePrefix;

}
