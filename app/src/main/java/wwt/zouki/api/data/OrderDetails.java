package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import wwt.zouki.model.Cart;

/**
 * Created by Ram Prasad on 9/17/2015.
 */
public class OrderDetails {
    public String account;

    @SerializedName("delivery_method")
    public String deliveryMethod;

    @SerializedName("customer_id")
    public String customerId;

    @SerializedName("wardname")
    public String wardName;

    @SerializedName("bed_number")
    public String bedNumber;

    @SerializedName("delivery_date")
    public String delivery_date;

    @SerializedName("delivery_name")
    public String name;

    @SerializedName("delivery_email")
    public String email;

    @SerializedName("delivery_mobile")
    public String mobile;

    @SerializedName("delivery_comment")
    public String comment;

    @SerializedName("total_price")
    public String totalPrice;

    @SerializedName("payment_method")
    public String methodOfPayment;


    @SerializedName("cartproducts")
    public List<Cart> cart;

    public Map<String, String> getFieldMap() {
        Map<String, String> map = new HashMap<>();

        map.put("account", account);
        map.put("customer_id", customerId);
        map.put("delivery_mobile", mobile);
        map.put("delivery_email", email);
        map.put("delivery_name", name);
        map.put("bed_number", bedNumber);
        map.put("wardname", wardName);
        map.put("delivery_comment", comment);
        map.put("delivery_date", delivery_date);
        map.put("payment_method", methodOfPayment);
        map.put("delivery_method", deliveryMethod);
        map.put("total_price", totalPrice);

        return map;
    }


}
