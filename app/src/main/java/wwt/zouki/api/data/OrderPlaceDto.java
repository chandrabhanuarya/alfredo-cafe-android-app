package wwt.zouki.api.data;

import java.io.Serializable;

/**
 * Created by sairam on 4/18/2016.
 */
public class OrderPlaceDto implements Serializable{

    public String account,delivery_method,wardname,delivery_name,delivery_email,delivery_mobile,delivery_comment,total_price,payment_method,cartproducts;
    public int customer_id;

    public String getDelivery_method() {
        return delivery_method;
    }

    public void setDelivery_method(String delivery_method) {
        this.delivery_method = delivery_method;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }

    public String getDelivery_name() {
        return delivery_name;
    }

    public void setDelivery_name(String delivery_name) {
        this.delivery_name = delivery_name;
    }

    public String getDelivery_email() {
        return delivery_email;
    }

    public void setDelivery_email(String delivery_email) {
        this.delivery_email = delivery_email;
    }

    public String getDelivery_mobile() {
        return delivery_mobile;
    }

    public void setDelivery_mobile(String delivery_mobile) {
        this.delivery_mobile = delivery_mobile;
    }

    public String getDelivery_comment() {
        return delivery_comment;
    }

    public void setDelivery_comment(String delivery_comment) {
        this.delivery_comment = delivery_comment;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getCartproducts() {
        return cartproducts;
    }

    public void setCartproducts(String cartproducts) {
        this.cartproducts = cartproducts;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
