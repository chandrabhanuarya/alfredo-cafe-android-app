package wwt.zouki.api.data;

/**
 * Created by Ram Prasad on 9/13/2015.
 */
public class PickUpInfo {
    public String name;
    public String phone;
    public String email;
}
