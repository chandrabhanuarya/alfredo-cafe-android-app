package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class Product {
    @SerializedName("id")
    public int id;

    @SerializedName("product_id")
    public int productId;

    @SerializedName("name")
    public String name;

    @SerializedName("pirce")
    public String price;

    @SerializedName("href")
    public String imageURL;

    @SerializedName("thumb")
    public String thumbnail;

    @SerializedName("special")
    public String special;

    @SerializedName("rating")
    public String rating;

    public int quantity = 10;
    public int beveragesQuantity = 1;
}
