package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class ProductDtls {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("manufacturer")
    public String manufacturer;

    @SerializedName("model")
    public String model;

    @SerializedName("reward")
    public String rewards;

    @SerializedName("points")
    public String points;

    @SerializedName("image")
    public String imageURL;

    @SerializedName("price")
    public String price;

    @SerializedName("special")
    public String special;

    @SerializedName("minimum")
    public int minimum;

    @SerializedName("rating")
    public int rating;

    @SerializedName("description")
    public String description;

    @SerializedName("options")
    public List<Option> options;

}
