package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class ProductInDetailResponse {
    @SerializedName("success")
    public boolean isSuccess;

    @SerializedName("product")
    public ProductDtls productDtls;
}
