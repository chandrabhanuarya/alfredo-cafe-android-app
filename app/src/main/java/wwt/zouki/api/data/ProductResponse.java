package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class ProductResponse {
    @SerializedName("success")
    public boolean isSuccess;

    @SerializedName("products")
    public List<Product> products;
}
