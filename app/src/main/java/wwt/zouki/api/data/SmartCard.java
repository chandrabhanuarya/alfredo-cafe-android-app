package wwt.zouki.api.data;

/**
 * Created by Ram Prasad on 8/21/2015.
 */
public class SmartCard {
    public String email;
    public String pinNumber;

    public SmartCard(String email, String pinNumber) {
        this.email = email;
        this.pinNumber = pinNumber;
    }
}
