package wwt.zouki.api.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 7/10/2015.
 */
public class User {

    @SerializedName("customer_id")
    public int customerId;

    @SerializedName("customer_group_id")
    public int customerGroupId;

    @SerializedName("store_id")
    public int storeId;

    @SerializedName("firstname")
    public String firstName;

    @SerializedName("lastname")
    public String lastName;

    @SerializedName("email")
    public String email;

    @SerializedName("telephone")
    public String phoneNo;

    @SerializedName("fax")
    public String fax;

    @SerializedName("password")
    public String password;

    @SerializedName("cart")
    public String cart;

    @SerializedName("address_id")
    public String addressId;

    @SerializedName("ip")
    public String ipAddress;

    @SerializedName("smartcard_number")
    public String smartCardNo;

}
