package wwt.zouki.api.data;

/**
 * Created by Arya on 10/07/2016.
 */
public class UserLogin {
    public String email;
    public String pinNumber;

    public UserLogin(String email, String pinNumber) {
        this.email = email;
        this.pinNumber = pinNumber;
    }
}
