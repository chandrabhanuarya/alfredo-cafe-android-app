package wwt.zouki.events;

import java.util.List;

import wwt.zouki.api.ApiError;
import wwt.zouki.api.data.Category;
import wwt.zouki.api.data.Product;
import wwt.zouki.api.data.ProductDtls;
import wwt.zouki.api.data.User;

/**
 * Generic base class for API response events
 * @param <T>
 */
public abstract class ApiResponseEvent<T> {
    protected T mTag;

    /**
     * Constructor with tag
     * @param tag The object provided by a specific API response
     */
    public ApiResponseEvent(T tag) {
        mTag = tag;
    }

    /**
     * Default constructor
     */
    public ApiResponseEvent() { }

    /**
     * Get object provided by a specific API response
     * @return The object returned by the API response
     */
    public T getTag() {
        T tag;
        return tag = mTag;
    }

    /**
     * Sets the object provided by an API response
     * @param item The object returned in an API response
     * @return An instance of the class, to facilitate method chaining
     */
    public ApiResponseEvent setTag(T item) {
        mTag = item;
        return this;
    }

    /**
     * Extended classes for each type of API response
     */
    public static final class Categories extends ApiResponseEvent<List<Category>> { }
    public static final class Products extends ApiResponseEvent<List<Product>> { }
    public static final class ProductInDetail extends ApiResponseEvent<ProductDtls> { }
    public static final class UserDetails extends ApiResponseEvent<User> { }
    public static final class Error extends ApiResponseEvent<ApiError> { }

    /**
     * API events, that do not contain any data
     */
    public static final class Success { }
}
