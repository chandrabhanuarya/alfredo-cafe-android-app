package wwt.zouki.events;

/**
 * Created by Ram Prasad on 7/10/2015.
 */
public class LoginRequestEvent {
    public String username;
    public String password;

    public LoginRequestEvent(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
