package wwt.zouki.events;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class ProductInDetailRequestEvent {
    public int itemId;

    public ProductInDetailRequestEvent(int itemId) {
        this.itemId = itemId;
    }
}
