package wwt.zouki.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.StringTokenizer;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.adapters.CategoryListAdapter;
import wwt.zouki.api.data.Category;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.events.CategoryRequestEvent;
import wwt.zouki.model.ServiceType;
import wwt.zouki.utils.ZoukiCart;
import wwt.zouki.utils.ZoukiConstants;

public class CategoryListFragment extends BaseFragment  {
    private static final int BURGER_ID = 59;
    private GridView listView;
    private String title;

    public CategoryListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            title = getArguments().getString(ZoukiConstants.CATEGORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_list, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchCategoryResponse();
    }

    @Override
    protected String getTitle() {
        return title + " MENU";
    }

    protected void fetchCategoryResponse() {
        showProgressDialog();

        BusProvider.getInstance().post(new CategoryRequestEvent(title));
    }

    @Subscribe
    public void onFeedApiResponse(ApiResponseEvent.Categories event) {
        buildCategoryList(event.getTag());
        hideProgressDialog();
    }

    protected void buildCategoryList(List<Category> categories) {
        CategoryListAdapter adapter = (CategoryListAdapter) listView.getAdapter();

        if (adapter != null) {
            adapter.updateCategories(categories);
        } else {
            adapter = new CategoryListAdapter(getActivity(), categories);
            listView.setAdapter(adapter);
        }
    }

    private boolean isOperatingOffers(String operatingHrs) {
        if (operatingHrs != null && !operatingHrs.equals("")) {
            SimpleDateFormat dt = new SimpleDateFormat("hh:mm a");

            StringTokenizer tokenizer = new StringTokenizer(operatingHrs, "-");
            Date current = new Date();

            try {
                int startHrs = dt.parse(tokenizer.nextToken()).getHours();
                int closeHrs = addMinutes(dt.parse(tokenizer.nextToken())).getHours();

                if (current.getHours() < startHrs || current.getHours() > closeHrs) {
                    return false;
                }
            } catch (ParseException pe) {
                pe.printStackTrace();
                return true;
            }
        }

        return true;
    }

    private Date addMinutes(Date date) {
        date.setTime(date.getTime() + (15 * 60 * 1000));
        return date;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        ((TextView) view.findViewById(R.id.titleTxt)).setText(title + "\t " + "MENU");
// Changes by Chandra
        if (EnumSet.of(ServiceType.CAFE).contains(ZoukiCart.getInstance().getServiceType())) {
            ((TextView) view.findViewById(R.id.timings_cafe))
                    .setText("\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  Operating Hours \n\n \t\t\t\t\t\t\t\t\t\t\t6:00 am – 7:30 pm [Pick Up]\n \t\t\t\t\t" +
                            "\t\t\t\t\t 9:00 am – 6:00 pm [Delivery]\n\n \t\t\t\t\t" +
                            "\t\t*Sandwich bar closes at 3:00 pm\n");

        }
//End
            listView = (GridView) view.findViewById(R.id.menu);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    checkForAlert((Category) listView.getItemAtPosition(position));
                }
            });

    }

    protected void checkForAlert(final Category category) {
        if (EnumSet.of(ServiceType.CATERING, ServiceType.CAKES, ServiceType.FLOWERS).contains(
                ZoukiCart.getInstance().getServiceType())) {
            navigateToNextScreen(category);
        } else {
            //showAlertDialog(getOperatingHours(category.name), new DialogInterface.OnClickListener() {
            //@Override
            //public void onClick(DialogInterface dialog, int which) {

            navigateToNextScreen(category);
            //  }
            //});
        }
    }

    private String getOperatingHours(String item) {
        return getString(getString(R.string.label_burgers).equalsIgnoreCase(item) ? R.string.title_burger_operating_hrs
                : R.string.title_cafe_operating_hrs);
    }

    protected void navigateToNextScreen(Category category) {
        Bundle bundle = new Bundle();
        Fragment fragment = null;
        int sandwich = Integer.parseInt("26");

        if (category.type.trim().equalsIgnoreCase("products")) {

            if(category.categoryId == BURGER_ID){
               // TextView header_title = (TextView) getView().findViewById(R.id.titleTxt);
               // header_title.setText("");
               // header_title.setText("BURGER MENU");

            }

            bundle.putBoolean("isBurger", category.parentId == BURGER_ID);
            bundle.putInt(ZoukiConstants.CATEGORY_ID, category.categoryId);
            bundle.putString(ZoukiConstants.TITLE, category.name);
            bundle.putString(ZoukiConstants.CATEGORY, title);

            fragment = new ProductListFragment();
        } else if (category.type.trim().equalsIgnoreCase("subcategory")) {


            if(category.categoryId == sandwich){

                bundle.putString(ZoukiConstants.CATEGORY, title);

                Log.d("sandwich clicked",String.valueOf(sandwich));
                if (mActivity != null) {
                    fragment = new SubCategoryListFragment();
                    mActivity.navigateToFragment(fragment, fragment.getClass().getName());
                }
            }
            else if(category.categoryId == BURGER_ID){

                bundle.putInt(ZoukiConstants.CATEGORY_ID, BURGER_ID);
                bundle.putString(ZoukiConstants.TITLE, "BURGER");
                bundle.putString(ZoukiConstants.CATEGORY, "BURGER");

             //   TextView header_title = (TextView) getView().findViewById(R.id.titleTxt);
             //   header_title.setText("");
             //   header_title.setText("BURGER MENU");

               // Log.d("burger clicked", String.valueOf(BURGER_ID) + header_title);
                if (mActivity != null) {
                    fragment = new SubCategoryListFragment();
                    mActivity.navigateToFragment(fragment, fragment.getClass().getName());
                }
            }
           else {
                //Log.d("Burger", String.valueOf(category.type.toString().trim().equalsIgnoreCase("subcategory")));
                Log.d("categoryid  burgerid", String.valueOf(category.categoryId) + "" + String.valueOf(BURGER_ID));

               // bundle.putBoolean("isBurger", category.categoryId == BURGER_ID);
                bundle.putInt(ZoukiConstants.CATEGORY_ID, category.categoryId);
                bundle.putString(ZoukiConstants.CATEGORY, title);
                bundle.putString(ZoukiConstants.TITLE, category.name);
                fragment = new SubCategoryListFragment();

            }

        }

       // bundle.putString(ZoukiConstants.CATEGORY, title);
        fragment.setArguments(bundle);

        if (mActivity != null) {
            mActivity.navigateToFragment(fragment, fragment.getClass().getName());
        }


    }

}
