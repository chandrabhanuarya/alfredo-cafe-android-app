package wwt.zouki.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;

public class CateringDescriptionFragment extends BaseFragment {

    public CateringDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_catering_description, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((TextView) getView().findViewById(R.id.name)).setText(getArguments()
                .getString("title"));

        WebView wv = (WebView) getView().findViewById(R.id.desc_catering);

        wv.getSettings();
        wv.setBackgroundColor(0x00000000);

        wv.loadData(getArguments().getString("description"), "text/html", "UTF-8");
    }
}
