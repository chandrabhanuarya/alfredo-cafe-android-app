package wwt.zouki.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.text.ParseException;

import retrofit.Callback;
import retrofit.RetrofitError;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.model.Card;
import wwt.zouki.utils.ZoukiCart;

public class ClientDetailsFragment extends ManagePaymentFragment {
    private EditText[] editTexts;

    public ClientDetailsFragment() {
        editTexts = new EditText[6];

        paymentType = "bill to company";
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_client_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();


        view.findViewById(R.id.login_acc).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                }
            }
        });


        final EditText password = (EditText) view.findViewById(R.id.reg_password);
        final EditText name = (EditText) view.findViewById(R.id.reg_Name);
        final EditText email = (EditText) view.findViewById(R.id.reg_emailId);
        final EditText phone = (EditText) view.findViewById(R.id.reg_phoneNo);

        view.findViewById(R.id.reg_submit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                validateClientDetails();
                //final View view = getView();

            }
        });



      /*  editTexts[0] = (EditText) view.findViewById(R.id.reg_Name);
        editTexts[1] = (EditText) view.findViewById(R.id.reg_emailId);

        editTexts[3] = (EditText) view.findViewById(R.id.reg_phoneNo);
        editTexts[4] = (EditText) view.findViewById(R.id.reg_password);

      /*  view.findViewById(R.id.reg_submit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                validateFields();
            }
        }); */
    }

    private void validateClientDetails() {

        Log.d("Checking client details", String.valueOf(getView().findViewById(R.id.reg_Name)));
        final EditText password = (EditText) getView().findViewById(R.id.reg_password);
        final EditText confirm_psd = (EditText) getView().findViewById(R.id.reg_confirm_psd);
        final EditText name = (EditText) getView().findViewById(R.id.reg_Name);
        final EditText email = (EditText) getView().findViewById(R.id.reg_emailId);
        final EditText phone = (EditText) getView().findViewById(R.id.reg_phoneNo);

        String reg_name = name.getText().toString();
        String reg_psd = password.getText().toString();
        String reg_email = email.getText().toString();
        String reg_phone = phone.getText().toString();
        String reg_confirm_psd = confirm_psd.getText().toString();


        if (!reg_name.equals("") && !reg_email.equals("") && !reg_phone.equals("") && !reg_psd.equals("") && !reg_confirm_psd.equals("")) {

            //showToast("Please enter all the fields!");
            ZoukiApi.getService().postRegistration(String.valueOf(name.getText().toString().trim()), String.valueOf(email.getText().toString().trim()), String.valueOf(password.getText().toString().trim()), String.valueOf(phone.getText().toString().trim()), new Callback<JsonObject>() {

                @Override
                public void success(JsonObject jsonObject, retrofit.client.Response response) {
                    // Log.d("Success", response.toString());
                    //Toast.makeText(n,response,Toast.LENGTH_LONG).show();

                    if (String.valueOf(jsonObject.get("success")).equals("true")) {

                        // showAlertDialog("Registration Successful!");
                        Intent i = new Intent(getActivity(), ManageAccountFragment.class);
// Starts TargetActivity
                        startActivity(i);
                       // showConfirmationAlert("Registration Successful!", 0, ClientDetailsFragment.this);
                       // mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());

                       // mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                        // Toast.makeText(getActivity(),"Registration Successful!", Toast.LENGTH_SHORT).show();

                    } else {
                        showAlertDialog("Registration Unsuccessful!");
                    }
                }

                public void failure(RetrofitError error) {
                    Log.d("TEST", "error : " + error.getMessage());
                    // hideProgressDialog();
                    showAlertDialog("Registration Unsuccessful!");
                    // mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
                }

            });

        } else {
            showToast("All fields are mandatory!");
            mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
        }
    }

    private void onSuccess() {
    }

    /* private void validateFields() {
        CompanyBill bill = getArguments().getParcelable("bill2company");

        if (!editTexts[0].getText().toString().equals("")
                && !editTexts[1].getText().toString().equals("")) {
            bill.clientName = editTexts[0].getText().toString();
            bill.clientEmail = editTexts[1].getText().toString();
            bill.title = editTexts[2].getText().toString();
            bill.department = editTexts[3].getText().toString();
            bill.clientPhone = editTexts[4].getText().toString();
            bill.fax = editTexts[5].getText().toString();

            BusProvider.getInstance().post(bill);
        } else {
            showToast("Please fill mandatory fields");
        }
    } */

    @Subscribe
    public void onApiResponse(ApiResponseEvent.Success event) throws ParseException {
        cartApi();
    }

    @Subscribe
    public void onErrorResponse(ApiResponseEvent.Error error) {
        showToast(error.getTag().message);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_client_details);
    }
}
