package wwt.zouki.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.data.CompanyBill;

public class CompanyAddressFragment extends BaseFragment {
    private EditText[] editTexts;

    public CompanyAddressFragment() {
        editTexts = new EditText[5];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_address, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        editTexts[0] = (EditText) view.findViewById(R.id.companyName);
        editTexts[1] = (EditText) view.findViewById(R.id.streetNumber);
        editTexts[2] = (EditText) view.findViewById(R.id.streetName);

        editTexts[3] = (EditText) view.findViewById(R.id.suburb);
        editTexts[4] = (EditText) view.findViewById(R.id.postCode);

        view.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    private void validateFields() {
        if (!editTexts[0].getText().toString().equals("")) {
            CompanyBill bill = new CompanyBill();
            bill.companyName = editTexts[0].getText().toString();
            bill.streetNo = editTexts[1].getText().toString();
            bill.streetName = editTexts[2].getText().toString();
            bill.suburb = editTexts[3].getText().toString();
            bill.postCode = editTexts[4].getText().toString();

            if (mActivity != null) {
                Fragment fragment = new ClientDetailsFragment();
                Bundle bundle = new Bundle();

                bundle.putParcelable("bill2company", bill);
                fragment.setArguments(bundle);

                mActivity.navigateToFragment(fragment, fragment.getClass().getName());
            }
        } else {
            showToast("Please fill mandatory details");
        }
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_company_address);
    }

}
