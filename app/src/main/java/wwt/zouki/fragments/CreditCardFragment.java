package wwt.zouki.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.*;

import com.google.gson.JsonObject;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.InputStream;
import java.text.ParseException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.model.Card;
import wwt.zouki.model.PaymentInfo;
import wwt.zouki.utils.PaymentInfoParser;
import wwt.zouki.utils.PaymentUtils;
import wwt.zouki.utils.ZoukiCart;


public class CreditCardFragment extends ManagePaymentFragment {
    private EditText cardNumber;
    private EditText cardName;
    private EditText expiryDate;
    private EditText cvvNumber;

    public float enter_promo_code;
    public float total_price;
    public String current_price, discount, final_total="null";

    final String TAG = getClass().getName();

    private Button scanButton;
    private TextView resultTextView;

    private int MY_SCAN_REQUEST_CODE = 100;


    public CreditCardFragment() {
        // Required empty public constructor

        paymentType = "credit card";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_credit_card, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //setContentView(R.layout.main);

        // resultTextView = (TextView) findViewById(R.id.resultTextView);
        scanButton = (Button) getView().findViewById(R.id.scan_credit_card);

        ((TextView) getView().findViewById(R.id.price_description)).setText("" + ZoukiCart
                .getInstance().getTotalCost());

        cardNumber = (EditText) getView().findViewById(R.id.card_number);
        cardName = (EditText) getView().findViewById(R.id.card_name);
        expiryDate = (EditText) getView().findViewById(R.id.expiry_date);
        cvvNumber = (EditText) getView().findViewById(R.id.cvv);

        cardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        expiryDate.addTextChangedListener(new DateFormatWatcher());


        getView().findViewById(R.id.smart_card_pay).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(mActivity!=null){
                    mActivity.navigateToFragment(new Delivery_SmartCard(), Delivery_SmartCard.class.getName());
                }

            }

        });

        getView().findViewById(R.id.apply_code).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                validateFields();
                if (getView().findViewById(R.id.enter_promo_code).toString() != null) {
                    validatePromoCode();
                } else {
                    showToast("Promo Code Field is Empty!");
                }

            }

        });

        getView().findViewById(R.id.scan_credit_card).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
                // scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
                creditCardScanner(v);
            }
        });

        getView().findViewById(R.id.payment).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                validateCardDetails();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                // cardNumber = cardNumber.setText(scanResult.getFormattedCardNumber().toString());
                EditText cardNo = (EditText) getView().findViewById(R.id.card_number);
                String ccNo = scanResult.getFormattedCardNumber().toString().replaceAll("\\s+", "");
                cardNo.setText(ccNo);

                //EditText expiryDate = (EditText) getView().findViewById(R.id.expiry_date);
                //expiryDate.setText(scanResult.expiryMonth +"/"+ scanResult.expiryYear);

                //EditText cardName = (EditText) getView().findViewById(R.id.card_name);
                //cardName.setText(scanResult.cardholderName);

                //  EditText cvv = (EditText) getView().findViewById(R.id.cvv);
                // cvv.setText(scanResult.cvv);

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";

                    EditText expiryDate = (EditText) getView().findViewById(R.id.expiry_date);

                    int year = scanResult.expiryYear;

                    String x_str = Integer.toString(year);

                    int new_year = Integer.parseInt(x_str.substring(2));

                    if (scanResult.expiryMonth < 10) {

                        expiryDate.setText("0" + scanResult.expiryMonth + "/" + new_year);
                    } else {
                        expiryDate.setText(scanResult.expiryMonth + "/" + new_year);
                    }

                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }


                if (scanResult.cardholderName != null) {
                    EditText cardName = (EditText) getView().findViewById(R.id.card_name);
                    cardName.setText(scanResult.cardholderName);
                }
            } else {
                resultDisplayStr = "Scan was canceled.";
            }


            Toast.makeText(this.getActivity(), resultDisplayStr, Toast.LENGTH_SHORT).show();
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    private void creditCardScanner(View v) {

    }

    private void validatePromoCode() {
        EditText enter_promo = (EditText) getView().findViewById(R.id.enter_promo_code);

        try {
            //validateFields();

            if (enter_promo != null) {

                if (mActivity != null) {

                    Log.v("Enter_promo_code", Float.valueOf(enter_promo_code).toString());

                    current_price = ZoukiCart.getInstance().getTotalCost().toString().replace("$", "");

                    CharSequence enter_promo_code = enter_promo.getText().toString();

                    Button apply_promo = (Button) getView().findViewById(R.id.apply_code);

                    Log.d("promo total price", current_price);
                    Log.d("checking promo code", enter_promo_code.toString());

                    ZoukiApi.getService().postPromoCode(enter_promo_code.toString(), current_price.toString(), new Callback<JsonObject>() {

                        @Override
                        public void success(JsonObject jsonObject, Response response) {

                            //Get the instance of JSONArray that contains JSONObjects
                            if (String.valueOf(jsonObject.get("success")).equalsIgnoreCase("true")) {

                                showToast("Processing...");
                                jsonObject.get("cupon_details");
                                jsonObject.getAsJsonObject("cupon_details").get("discount");

                                String discount_value = String.valueOf(jsonObject.getAsJsonObject("cupon_details").get("discount"));
                                discount_value = discount_value.replace("\"", "");

                                Log.d("discount value", discount_value);
                                // Log.d("json data discount", String.valueOf(jsonObject.getAsJsonObject("cupon_details").get("discount")));

                                String x_str = current_price.substring(0, Math.min(current_price.length(), 6));

                                Log.d("current prce ....x_str", current_price + x_str);

                                discount = String.valueOf((Float.parseFloat(x_str)* Float.parseFloat(discount_value))/100);

                                // discount = String.valueOf((Float.valueOf(x_str) * Float.valueOf(discount_value)) / 100);

                                Log.d("discount value", discount);

                                total_price = Float.parseFloat(String.valueOf(Float.valueOf(x_str) - Float.valueOf(discount)));

                                Log.d("After Discount Price", String.valueOf(total_price));

                                //JSONObject promoValue = String.valueOf(jsonObject.getAsJsonObject("discount"));
                                float promoValue;

                                TextView cp = (TextView) getView().findViewById(R.id.price_description);
                                cp.setText("$" + String.valueOf(total_price));
                                showToast("Promo Code Applied!");
                                Log.d("Promo checking  value", String.valueOf(total_price));
                                // enter_promo.setText("Applied");
                                final_total =String.valueOf(total_price);

                                final_total = final_total.substring(0, Math.min(final_total.length(), 6));
                                Log.d("final_total", final_total);

                                getView().findViewById(R.id.enter_promo_code).setFocusable(false);

                                getView().findViewById(R.id.apply_code).setClickable(false);
                                // Changing the current price total after applying promo code
                                ((TextView) getView().findViewById(R.id.price_description)).setText("$" + final_total);
                                // showToast("New Password has been sent to your E-mail address!");
                            } else {
                                showToast("Invaild Pormo Code!");
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("PromoCode Fail!", error.toString());
                        }
                    });


                    // Changing the current price total after applying promo code
                    ((TextView) getView().findViewById(R.id.price_description)).setText("$" + total_price);
                }

            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    private void validateFields() {
        EditText editText = (EditText) getView().findViewById(R.id.enter_promo_code);
        if (editText.getText().toString().equals("")) {
            showToast("Promo Code Field is Empty!");
        }
    }


    private void validateCardDetails() {
        String number = cardNumber.getText().toString().replace(" ", "");
        String expiry = expiryDate.getText().toString();

        String name = cardName.getText().toString();
        String cvv = cvvNumber.getText().toString();

        if (!number.equals("") && !name.equals("") && !expiry.equals("")) {
            showToast("Processing....");
            Card card = new Card();
            card.setCardNumber(number);
            card.setCardName(name);
            card.setExpiryDate(expiry);
            card.setCvv(cvv);
            ZoukiCart.getInstance().setCard(card);
            new SecurePayTask().execute();
        } else {
            showToast("Please fill all the details before proceeding for Payment");
        }
    }


    /**
     * Formats the watched EditText to a credit card number
     */
    public static class FourDigitCardFormatWatcher implements TextWatcher {
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);

                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);

                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }

    /**
     * Formats the watched EditText to a expiry date
     */
    public static class DateFormatWatcher implements TextWatcher {
        private static final char space = '/';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 3) == 0) {
                final char c = s.charAt(s.length() - 1);

                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 3) == 0) {
                char c = s.charAt(s.length() - 1);

                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 1) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }

    private class SecurePayTask extends AsyncTask {
        private PaymentInfo paymentInfo;

        @Override
        protected Object doInBackground(Object[] params) {
            // Test Server: https://test.securepay.com.au/xmlapi/payment
            HttpPost httppost = new HttpPost("https://test.securepay.com.au/xmlapi/payment");
            HttpClient httpclient = new DefaultHttpClient();

            try {
                StringEntity se = new StringEntity(new PaymentUtils()
                        .getXML(), HTTP.UTF_8);

                se.setContentType("text/xml");
                httppost.setEntity(se);
                Log.d("XML", se.toString());
                HttpResponse httpresponse = httpclient.execute(httppost);
                InputStream inputStream = httpresponse.getEntity().getContent();


                paymentInfo = getPaymentDtls(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        private PaymentInfo getPaymentDtls(InputStream inputStream) throws Exception {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            PaymentInfoParser handler = new PaymentInfoParser();
            saxParser.parse(inputStream, handler);

            return handler.getPaymentInfo();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            if (paymentInfo != null && paymentInfo.isApproved()) {
                Log.d("TEST", "Approved : " + paymentInfo.isApproved());
                try {
                    cartApi();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                if (paymentInfo != null) {
                    if (paymentInfo.getResponseText() != null) {
                        String responseText = paymentInfo.getResponseText();
                        showToast(responseText);
                    } else {
                        showToast("Some thing went wrong please try again...");
                    }
                } else {
                    showToast("Some thing went wrong please try again...");
                }
            }
        }

    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_credit_card);
    }


}
