package wwt.zouki.fragments;

/**
 * Created by Arya on 1/08/2016.
 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.MainActivity;
import wwt.zouki.R;
import wwt.zouki.adapters.OptionListAdapter;
import wwt.zouki.api.data.Option;
import wwt.zouki.api.data.ProductDtls;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.events.ProductInDetailRequestEvent;
import wwt.zouki.interfaces.AddOnEventListener;
import wwt.zouki.model.Cart;
import wwt.zouki.model.OptionItem;
import wwt.zouki.model.ServiceType;
import wwt.zouki.utils.ApplicationUtils;
import wwt.zouki.utils.ZoukiCart;
import wwt.zouki.utils.ZoukiConstants;
import wwt.zouki.views.OptionsListView;

public class CustomizeItemFragment extends BaseFragment implements AddOnEventListener {
    private int MIN_HEAD_COUNT;
    public boolean isBurger;

    private OptionsListView optionList;
    private WebView description;
    private TextView quantity;
    private ImageView display;
    private TextView name;
    private TextView cost;
    private Cart item;
    private String descTxt;



    public CustomizeItemFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MIN_HEAD_COUNT = ZoukiCart.getInstance().isOnCatering() ? 10 : 1;

        if (ZoukiCart.getInstance().isOnCatering()) {
            MIN_HEAD_COUNT = getArguments().getInt("quantity");
        }

        item = new Cart();
        item.serviceType = ZoukiCart.getInstance()
                .getServiceType();

        item.quantity = MIN_HEAD_COUNT;
        item.optionDesc = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customize_item, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchProductDetails();
    }

    private void fetchProductDetails() {
        showProgressDialog();

        BusProvider.getInstance().post(new ProductInDetailRequestEvent(
                getArguments().getInt(ZoukiConstants.PRODUCT_ID)));
    }

    @Subscribe
    public void onApiResponse(ApiResponseEvent.ProductInDetail event) {
        hideProgressDialog();
        ProductDtls productDtls = event.getTag();

        isBurger = productDtls.model != null && productDtls
                .model.equals("Burgers");

        item.type = getArguments().getString(ZoukiConstants.CATEGORY);
        item.productId = productDtls.id;
        item.name = productDtls.name;

        item.price = Float.parseFloat(productDtls.price.replace("$", ""));
        item.total = item.price * item.quantity;

        //UPDATE PRODUCT
        cost.setText("$" + ApplicationUtils.roundOffValue(item.total));
        descTxt = productDtls.description;

        name.setText(item.name);
        item.imageURL = ApplicationUtils.getWellFormedURL(productDtls.imageURL);

        if (!ZoukiCart.getInstance().isOnCatering()) {
            Picasso.with(getActivity()).load(item.imageURL).error(R.mipmap.iconsmall)
                    .into(display);
        } else {
            if (descTxt.equals("")) {
                getView().findViewById(R.id.cateringLayout).setVisibility(View.GONE);
            } else {
                description.loadData(descTxt, "text/html", "UTF-8");
            }
        }

        buildOptionList(productDtls.options);
        getView().findViewById(R.id.add_to_cart).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!optionList.isMandatoryItemsMissed()) {
                    new AddItemAsyncTask(item).execute();
                } else {
                    showAlertDialog(getString(R.string.lbl_required));
                }
            }
        });
    }

    private class AddItemAsyncTask extends AsyncTask {
        private Cart cart;

        public AddItemAsyncTask(Cart cart) {
            this.cart = cart;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            cart.save();

            if (cart.options != null) {
                for (OptionItem item : cart.options) {
                    item.cart = cart;
                    item.save();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            hideProgressDialog();

            Toast.makeText(getActivity(), getString(R.string.label_item_added), Toast.LENGTH_SHORT).show();
            ((MainActivity) getActivity()).initBadges();

            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    private void buildOptionList(List<Option> options) {
        OptionListAdapter adapter = (OptionListAdapter) optionList.getAdapter();

        if (adapter != null) {
            adapter.updateOptions(options);
        } else {
            adapter = new OptionListAdapter(getActivity(), options, this);
            optionList.setAdapter(adapter);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        display = (ImageView) view.findViewById(R.id.img_product);
        display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.imageURL != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("imageURL", item.imageURL);

                    if (descTxt != null) {
                        bundle.putString("description", descTxt);
                    }

                    BaseFragment fragment = new ItemSummaryFragment();
                    fragment.setArguments(bundle);

                    mActivity.navigateToFragment(fragment, fragment.getTag());
                }
            }
        });

        name = (TextView) view.findViewById(R.id.name);
        cost = (TextView) view.findViewById(R.id.cost);

        optionList = (OptionsListView) view.findViewById(R.id.option_list);

        view.findViewById(R.id.increment).setOnClickListener(new CustomizeOrderListener(true));
        view.findViewById(R.id.decrement).setOnClickListener(new CustomizeOrderListener(false));

        quantity = (TextView) view.findViewById(R.id.quantity);
        quantity.setText(String.valueOf(MIN_HEAD_COUNT));



        if (ZoukiCart.getInstance().isOnCatering()) {
            view.findViewById(R.id.cateringLayout).setVisibility(View.VISIBLE);
            view.findViewById(R.id.desc_others).setVisibility(View.GONE);

            view.findViewById(R.id.lbl_min_qnt).setVisibility(View.VISIBLE);
            description = (WebView) view.findViewById(R.id.desc_catering);

            description.getSettings();
            description.setBackgroundColor(0x00000000);

            //   getView().findViewById(R.id.descLayout).setOnClickListener(clickListener);
            // getView().findViewById(R.id.next).setOnClickListener(clickListener);
        } else if (ZoukiCart.getInstance().getServiceType().getTag().contains("Burgers")) {
            Log.d("Burgers triggered", String.valueOf(ZoukiCart.getInstance().getServiceType().getTag().contains("Burgers")));
            view.findViewById(R.id.cateringLayout).setVisibility(View.VISIBLE);
            view.findViewById(R.id.desc_others).setVisibility(View.GONE);
            description = (WebView) view.findViewById(R.id.desc_catering);

            description.getSettings();
            description.setBackgroundColor(0x00000000);

        }

        getView().findViewById(R.id.help_text).setVisibility(item.serviceType == ServiceType.CATERING
                ? View.GONE : View.VISIBLE);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mActivity != null) {
                Fragment fragment = new CateringDescriptionFragment();
                Bundle bundle = new Bundle();

                bundle.putString("title", name.getText().toString());
                bundle.putString("desc", descTxt);

                fragment.setArguments(bundle);
                mActivity.navigateToFragment(fragment, fragment.getClass().getName());
            }
        }
    };

    private class CustomizeOrderListener implements View.OnClickListener {
        private boolean increment;

        public CustomizeOrderListener(boolean increment) {
            this.increment = increment;
        }

        @Override
        public void onClick(View v) {
            if (increment) {
                item.quantity++;
            } else {
                if (item.quantity > MIN_HEAD_COUNT) {
                    item.quantity--;
                }
            }

            item.total = item.price * item.quantity;

            quantity.setText(String.valueOf(item.quantity));
            cost.setText("$" + ApplicationUtils.roundOffValue(item.total));
        }
    }

    @Override
    protected String getTitle() {
        return getArguments().getString(ZoukiConstants.TITLE);
    }

    @Override
    public void updatePrice(OptionItem option, boolean isChecked) {
        try {
            Float addedItem = Float.parseFloat(option.price.replace("$", ""));
//          item.price = item.price + (isChecked ? +addedItem : -addedItem);

            if (isChecked) {
                item.price = item.price + addedItem;
            } else {
                item.price = item.price - addedItem;
            }

            item.total = item.price * item.quantity;
            cost.setText("$" + ApplicationUtils.roundOffValue(item.total));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } finally {
            if (item.options == null) item.options = new ArrayList<>();
            item.options.add(option);

            //TODO: Need to clean it off once conformed
            item.optionDesc = item.optionDesc + (!item.optionDesc.equals("") ? ", "
                    : "") + option.value;
        }
    }

    @Override
    public void updatePrice(OptionItem option, String cur, String prev) {
        try {
            Float currPrice = Float.parseFloat(cur.replace("$", ""))
                    * item.quantity;

            if (prev != null) {
                item.price = item.price + currPrice - Float.parseFloat(prev
                        .replace("$", ""));
            } else {
                item.price = item.price + currPrice;
            }

            item.total = item.price * item.quantity;
            cost.setText("$" + ApplicationUtils.roundOffValue(item.total));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        } finally {
            item.optionDesc = option.value;
            replaceOption(option);
        }
    }

    @Override
    public void updateComment(OptionItem option) {
        replaceOption(option);
    }

    @Override
    public void updateDateTime(int id, OptionItem option) {
        // UPDATE DATE
        replaceOption(option);
    }

    private void replaceOption(OptionItem option) {
        if (item.options == null) item.options = new ArrayList<>();
        int pos = getItemPosition(option.type);

        if (pos != -1) {
            item.options.add(pos, option);
        } else {
            item.options.add(option);
        }
    }

    private int getItemPosition(String type) {
        int pos = -1;

        for (int i = 0; i < item.options.size(); i++) {
            OptionItem op = item.options.get(i);

            if (op.type.equals(type)) {
                pos = i;
            }
        }

        return pos;
    }
}
