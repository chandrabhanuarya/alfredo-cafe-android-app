package wwt.zouki.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.model.DeliveryDetails;
import wwt.zouki.model.ValidationException;
import wwt.zouki.utils.ApplicationUtils;
import wwt.zouki.utils.ZoukiCart;

public class DeliveryAddressFragment extends BaseFragment {
    private EditText[] editTexts;

    public DeliveryAddressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        editTexts = new EditText[4];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delivery_address, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        editTexts[0] = (EditText) getView().findViewById(R.id.name);
        editTexts[1] = (EditText) getView().findViewById(R.id.email);
        editTexts[2] = (EditText) getView().findViewById(R.id.phoneNo);
        editTexts[3] = (EditText) getView().findViewById(R.id.address);

        getView().findViewById(R.id.pickup_option).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new DeliveryOptionsFragment(), DeliveryOptionsFragment.class.getName());
                }
            }
        });

        getView().findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    validateFields();
                    setDeliveryAddress();


                    Log.d("Tag Name", ZoukiCart.getInstance().getServiceType().getTag().toString());

                    if (ZoukiCart.getInstance().getServiceType().getTag().equals("catering")) {
                        mActivity.navigateToFragment(new CostCenterFragment(), CostCenterFragment
                                .class.getName());

                    } else
                    {
                        mActivity.navigateToFragment(new CreditCardFragment(), CreditCardFragment.class.getName());
                    }
                } catch (Exception e) {
                    showToast(e.getMessage());
                }
            }
        });
    }

    private void validateFields() {
        for (EditText editText : editTexts) {
            if (editText.getText().toString().equals("")) {
                throw new ValidationException("All Fields Are Mandatory.");
            } else if (!ApplicationUtils.isEmailValid(editTexts[1].getText().toString())) {
                throw new ValidationException("Entered Email is Not Valid.");
            }
        }
    }

    private void setDeliveryAddress() {
        DeliveryDetails details = new DeliveryDetails();
        details.setAddress(editTexts[3].getText().toString());
        details.setMobile(editTexts[2].getText().toString());
        details.setEmail(editTexts[1].getText().toString());
        details.setName(editTexts[0].getText().toString());

        ZoukiCart.getInstance().setDeliveryDtls(details);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_delivery_address);
    }
}
