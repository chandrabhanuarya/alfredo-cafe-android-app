package wwt.zouki.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.model.Cart;
import wwt.zouki.model.ValidationException;
import wwt.zouki.utils.ZoukiCart;

public class DeliveryDetailsFragment extends BaseFragment {
    protected static final int TIME_DIALOG_ID = 999;
    protected static final int DATE_DIALOG_ID = 998;

    private boolean isDelivery;

    private int day, month, year;
    private int hour, minute,second;
    private boolean isPickUp;

    private Calendar calendar;
    private Calendar selected;

    private TextView timePicker;
    private TextView datePicker;

    private ZoukiCart zoukiCart;
    private EditText addressEdt;

    public String timeSelected;
    public String dateSelected;

    public DeliveryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            isPickUp = getArguments().getString("payment_type").equals("pickup");
        }

        zoukiCart = ZoukiCart.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_delivery_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addressEdt = (EditText) getView().findViewById(R.id.address);
        addressEdt.setVisibility(isPickUp ? View.GONE : View.VISIBLE);

        calendar = Calendar.getInstance();
        selected = Calendar.getInstance();

        // Render View
        View view = getView();

        view.findViewById(R.id.pickup_option).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new DeliveryOptionsFragment(), DeliveryOptionsFragment.class.getName());
                }
            }
        });

        adjustCalendar();
        selected.setTime(calendar.getTime());

        timePicker = (TextView) view.findViewById(R.id.timePicker);
        datePicker = (TextView) view.findViewById(R.id.datePicker);


        datePicker.setText(calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1)
                + "-" + calendar.get(Calendar.DATE));

        timePicker.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE)+ ":" + calendar.get(Calendar.SECOND)
                 +" " +(calendar.get(Calendar.AM_PM) == 0 ? "AM" : "PM"));

        view.findViewById(R.id.timePickerLt).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createDialog(TIME_DIALOG_ID).show();
            }
        });

        view.findViewById(R.id.datePickerLt).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createDialog(DATE_DIALOG_ID).show();
            }
        });


        view.findViewById(R.id.checkout).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    validateFields();
                   // ZoukiCart.getInstance().setDelivery(isDelivery);
                    Bundle bundle = new Bundle();


                    Fragment fragment;

                    fragment = new DeliveryAddressFragment();

                    if (addressEdt != null) {

                        if (mActivity != null) {

                            ZoukiCart.getInstance().setDeliveryTime(timePicker.getText().toString());
                            ZoukiCart.getInstance().setDeliveryDate(datePicker.getText().toString());
                            //Checking if the min amount is $20
                            if (isMinAmnt()) {
                                bundle.putString("payment_type", "delivery");
                                fragment.setArguments(bundle);
                                timeSelected = timePicker.getText().toString();
                                dateSelected = datePicker.getText().toString();

                                mActivity.navigateToFragment(fragment, fragment.getClass().getName());
                            } else {
                                showAlertDialog(getString(R.string.warning_min_amnt));
                            }
                        }
                    }
                } catch (Exception e) {
                    showToast(e.getMessage());
                }
            }
        });
    }


    private void validateFields() {
        EditText editText = (EditText) getView().findViewById(R.id.address);
        if (editText.getText().toString().equals("")) {
            throw new ValidationException("All Fields Are Mandatory.");
        }
    }

    public Dialog createDialog(int id) {
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        second = calendar.get(Calendar.SECOND);

        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                //Changes: Chandra Bhanu.P changes true to false to make sure the clock is in 12 hour display
                return new TimePickerDialog(getActivity(), timePickerListener, hour, minute, false);

            case DATE_DIALOG_ID:
                // set time picker as current time
                //DatePickerDialog dialog =  new DatePickerDialog(getActivity(), datePickerListener,
                // year, month, day);
                return new DatePickerDialog(getActivity(), datePickerListener, year, month, day);
            // Changes: Chandra Bhanu.P  commented the two lines to make sure, calendar displays full month
            // dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            // return dialog;
        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    selected.set(Calendar.HOUR_OF_DAY, selectedHour);
                    selected.set(Calendar.MINUTE, selectedMinute);

                    // showAlertDialog("time selected ",selected.getTime());

                    if (selected.getTime().before(calendar.getTime())) {
                        showAlertDialog(getString(R.string.date_time_validation_msg));

                    } else {
                        hour = selectedHour;
                        minute = selectedMinute;
                        int selectedSecond = second;

                        // set current time
                        String deliveryTime = new StringBuilder().append(pad(hour > 12 ? hour - 12 : hour))
                                .append(":").append(pad(minute)).append(":").append(pad(selectedSecond)) +" " + (hour > 12 ? "PM" : "AM");

                       // String display_time = new StringBuilder().append(pad(hour > 12 ? hour - 12 : hour))
                        //        .append(":").append(pad(minute)) +" " + (hour > 12 ? "PM" : "AM");

                        ZoukiCart.getInstance().setDeliveryTime(deliveryTime);
                        Log.d("time in DD", deliveryTime);
                        timePicker.setText(deliveryTime);
                        
                    }

                }
            };

    private void adjustCalendar() {
        switch (zoukiCart.getServiceType()) {
            case CAFE:
                calendar.add(Calendar.MINUTE, 30);
                break;
            case CAKES:
                calendar.add(Calendar.HOUR_OF_DAY, 24);
                break;
            case CATERING:
                calendar.add(Calendar.DATE, 2);
                break;
            case FLOWERS:
                calendar.add(Calendar.HOUR_OF_DAY, 6);
                break;

            default:
        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int years, int monthOfYear,
                                      int dayOfMonth) {
                    selected.set(years, monthOfYear, dayOfMonth);

                  //  if (selected.getTime().before(calendar.getTime())) {
                   //     showAlertDialog(getString(R.string.date_time_validation_msg));
                   // } else {
                        day = dayOfMonth;
                        month = monthOfYear;
                        year = years;

                        // set current date
                       // String display_Date = new StringBuilder().append(pad(day))
                         //       .append("/").append(pad(month + 1)).append("/").append(pad(year)).toString();


                    String deliveryDate = new StringBuilder().append(pad(year))
                            .append("-").append(pad(month + 1)).append("-").append(pad(day)).toString();

                    String display_date = new StringBuilder().append(pad(day))
                            .append("-").append(pad(month + 1)).append("-").append(pad(year)).toString();

                    ZoukiCart.getInstance().setDeliveryDate(deliveryDate);
                        Log.d("date in DD", deliveryDate);
                        datePicker.setText(deliveryDate);
                   // }
                }
            };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_delivery_details);
    }


    private boolean isMinAmnt() {
        List<Cart> items = ZoukiCart.getInstance().getCartList();
        float cost = 0;

        for (Cart item : items) {
            cost = cost + item.total;
        }
        return cost > 20;
    }

}
