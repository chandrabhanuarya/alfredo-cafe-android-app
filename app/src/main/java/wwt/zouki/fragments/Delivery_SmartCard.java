package wwt.zouki.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.text.ParseException;

import javax.security.auth.callback.Callback;

import retrofit.RetrofitError;
import retrofit.client.Response;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.api.data.GenericResponse;
import wwt.zouki.api.data.SmartCard;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.utils.ZoukiCart;

public class Delivery_SmartCard extends ManagePaymentFragment {

    public Delivery_SmartCard() {
        // Required empty public constructor

        paymentType = "smart card";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_smart_card, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((TextView) getView().findViewById(R.id.price_description)).setText("" + ZoukiCart
                .getInstance().getTotalCost());



        getView().findViewById(R.id.credit_card_pay).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new CreditCardFragment(), CreditCardFragment.class.getName());
                }
            }
        });

        getView().findViewById(R.id.payment).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showToast("Processing....");
                validateSmartCard();
            }
        });
    }

    private void validateSmartCard() {
        String smartPIN = ((EditText) getView().findViewById(R.id.smart_card_no))
                .getText().toString();

        String emailId = ((EditText) getView().findViewById(R.id.emailId))
                .getText().toString();

        if (!smartPIN.equals("") && !emailId.equals("")) {

         /*   ZoukiApi.getService().checkSmartCard(emailId, smartPIN, new retrofit.Callback<GenericResponse>() {


                @Override
                public void success(GenericResponse genericResponse, Response response) {
                    Log.d("Response Smart Card", response.toString());
                    Log.d("resp smart c", genericResponse.toString());

                }

                @Override
                public void failure(RetrofitError error) {
                    showToast("Smart Card Incorrect!");
                }
            });

*/
            Log.d("delivery smart card", emailId);
            BusProvider.getInstance().post(new SmartCard(emailId, smartPIN));
        } else {
            showToast("Please fill all the details");
        }
    }

    @Subscribe
    public void onApiResponse(ApiResponseEvent.Success success) throws ParseException {
        cartApi();
    }

    @Subscribe
    public void onErrorResponse(ApiResponseEvent.Error error) {
        showToast(error.getTag().message);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_smart_card);
    }

}
