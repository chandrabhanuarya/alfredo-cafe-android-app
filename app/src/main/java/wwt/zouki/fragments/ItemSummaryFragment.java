package wwt.zouki.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import wwt.zouki.fragments.RoundedTransformation;
import wwt.zouki.BaseFragment;
import wwt.zouki.R;

/**
 * Created by Ram Prasad on 12/2/2015.
 */
public class ItemSummaryFragment extends BaseFragment {
   // private ImageView imageView;
    private TextView textView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_summary, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       // imageView = (ImageView) getView().findViewById(R.id.imageView);
        textView  = (TextView)  getView().findViewById(R.id.burger_desp);
        ScrollView burger_des = (ScrollView)  getView().findViewById(R.id.burger_text);
        Bundle bundle = getArguments();
        if (bundle != null) {
           // String imageURL = bundle.getString("imageURL");
        /*    if (imageURL != null) {
                Picasso.with(getActivity()).load(imageURL).error(R.mipmap.icon_small)
                        .into(imageView);
            }  */
            Log.d("burger clicked", String.valueOf(bundle.getString("description")));

            String desc = bundle.getString("description");
            if (desc != null) {
                burger_des.setVisibility(View.VISIBLE);
                textView.setText(Html.fromHtml(desc));

            }
        }
    }
}
