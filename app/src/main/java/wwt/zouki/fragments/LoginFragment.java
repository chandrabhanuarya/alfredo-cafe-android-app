package wwt.zouki.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import wwt.zouki.api.data.UserLogin;

import com.google.gson.JsonObject;
import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.api.data.SmartCard;
import wwt.zouki.events.BusProvider;


public class LoginFragment extends BaseFragment {
    public EditText userEdt, pswdEdt;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Password = "passwordKey";
    public static final String Email = "emailKey";
    SharedPreferences sharedpreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_account, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View view = getView();
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        view.findViewById(R.id.login_acc).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                }
            }
        });
        view.findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new fragment_forgotpassword(), fragment_forgotpassword.class.getName());
                }
            }
        });

        view.findViewById(R.id.account_reg).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
                }
            }
        });

        userEdt = (EditText) view.findViewById(R.id.username);
        pswdEdt = (EditText) view.findViewById(R.id.password);






        view.findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                try {
                    Log.d("logg", String.format("%s%s", userEdt.getText().toString(), pswdEdt.getText().toString()));

                    validateLogin();

                } catch (Exception e) {
                    showToast(e.getMessage());
                }
            }
        });
    }

    private void validateLogin() {

        String username = userEdt.getText().toString();
        String password = pswdEdt.getText().toString();

        final LinearLayout login_form = (LinearLayout) getView().findViewById(R.id.login_form);



        if (username.trim().length() > 0 && password.trim().length() > 0) {
            BusProvider.getInstance().post(new UserLogin(username, password));

        } else {
            showToast("Please Enter Username & Password!");
            mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
        }
    }


    @Override
    protected String getTitle() {
        return getString(R.string.title_manage_account);
    }
}
