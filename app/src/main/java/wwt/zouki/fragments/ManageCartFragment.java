package wwt.zouki.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.MainActivity;
import wwt.zouki.R;
import wwt.zouki.adapters.CartListAdapter;
import wwt.zouki.interfaces.CartEventListener;
import wwt.zouki.model.Cart;
import wwt.zouki.utils.ZoukiCart;

public class ManageCartFragment extends BaseFragment implements CartEventListener {
    private ListView mListView;
    private TextView cartEmpty;
    private List<Cart> cart;
    private TextView cartValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_cart, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        mListView = (ListView) view.findViewById(R.id.cartList);
        cartEmpty = (TextView) view.findViewById(R.id.noItemsFound);
        cartValue = (TextView) view.findViewById(R.id.label_cost);

        view.findViewById(R.id.checkout).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mActivity != null && !isCartEmpty()) {
                    mActivity.navigateToFragment(new DeliveryOptionsFragment(), DeliveryOptionsFragment.class.getName());
                } else {
                    mActivity.showAlert("Please add something to cart before selecting checkout", null);
                }
            }
        });


    }

    private boolean isCartEmpty() {
        return ZoukiCart.getInstance().getCartList().isEmpty();
    }

    @Override
    public void onResume() {
        super.onResume();

        cartValue.setText("Total Price - " + ZoukiCart.getInstance()
                .getTotalCost());

        buildCartList();
    }

    private void buildCartList() {
        CartListAdapter adapter = (CartListAdapter) mListView.getAdapter();
        cart = ZoukiCart.getInstance().getCartList();

        mListView.setVisibility(View.VISIBLE);
        cartEmpty.setVisibility(View.GONE);

        if (adapter != null && !cart.isEmpty()) {
            adapter.updateCartList(cart);
        } else if (!cart.isEmpty()) {
            adapter = new CartListAdapter(getActivity(), cart, this);
            mListView.setAdapter(adapter);
        } else {
            cartEmpty.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_cart_page);
    }

    @Override
    public void deleteItem(int position) {
        new DeleteItemFromCart(position).execute();
    }

    @Override
    public void onCartUpdated() {
        cartValue.setText("Total Price - " + ZoukiCart.getInstance()
                .getTotalCost());
    }

    private class DeleteItemFromCart extends AsyncTask {
        private int position;

        public DeleteItemFromCart(int position) {
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            cart.get(position).delete();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            ((MainActivity) getActivity()).initBadges();

            hideProgressDialog();
            buildCartList();
            onCartUpdated();
        }
    }
}
