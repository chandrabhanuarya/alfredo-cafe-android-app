package wwt.zouki.fragments;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.api.ZoukiService;
import wwt.zouki.api.data.OrderDetails;
import wwt.zouki.api.data.PickUpInfo;
import wwt.zouki.api.data.User;
import wwt.zouki.interfaces.AlertListener;
import wwt.zouki.model.Cart;
import wwt.zouki.model.DeliveryDetails;
import wwt.zouki.model.OptionItem;
import wwt.zouki.utils.ZoukiCart;

/**
 * Created by Ram Prasad on 9/25/2015.
 */
public class ManagePaymentFragment extends BaseFragment implements AlertListener {
    protected String paymentType;

    protected void cartApi() throws ParseException {
        final ZoukiCart zCart = ZoukiCart.getInstance();
        User user = zCart.getUserDtls();

        final OrderDetails order = new OrderDetails();
        order.account = user != null ? "register" : "guest";
        order.comment = "";

        order.customerId = user != null ? String.valueOf(user.customerId) : "";
        // order.deliveryMethod = zCart.isDelivery() ? "delivery" : "pickup";


            order.deliveryMethod = "delivery";

            DeliveryDetails deliveryInfo = zCart.getDeliveryDtls();
            final String address = deliveryInfo.getWardName();
            order.mobile = deliveryInfo.getMobile();
            order.wardName = address;
            order.email = deliveryInfo.getEmail();
            order.name = deliveryInfo.getName();


        // 1970-01-01 00:00:00


        order.methodOfPayment = paymentType;
        order.totalPrice = ZoukiCart.getInstance().getTotalCost();
        String date=null;

        //Log.d("PickupPayment Frag", order.email+ " "+order.deliveryMethod);

        final List<Cart> cart = zCart.getCartList();
        for (Cart cartItem : cart) {
            date= order.delivery_date = zCart.getDeliveryDate() + " " + zCart.getDeliveryTime();
            //   date= cartItem.delivery_date = zCart.getDeliveryDate()+ " " + "00:00:00";
            // Log.d("Date", date);
            cartItem.fetchOptions();
            Log.d("burger comment", order.comment);
            Log.d("burger comments check", cartItem.optionDesc);

        }

        showProgressDialog();
        order.cart = cart;
        //  Log.d("Date Email final check", date + " " + order.email + " " + order.deliveryMethod);
        //  Log.d("Shipping", order.deliveryMethod);


        final String finalDate = date.replace("AM", "").replace("PM",""),finals=null;


        class GetNotificationsDetails extends AsyncTask<String, Void, String> {
            String response_string="";
            protected String doInBackground(String... params) {

                try {
                    JSONObject jsonParam = new JSONObject();

                    jsonParam.put("delivery_date", finalDate ==null? "" : finalDate);
                    jsonParam.put("account", order.account==null?"":order.account);
                    jsonParam.put("delivery_method",  order.deliveryMethod==null?"": order.deliveryMethod);
                    jsonParam.put("customer_id", order.customerId==null?"":order.customerId);
                    jsonParam.put("wardname", order.wardName==null?"":order.wardName);
                    jsonParam.put("delivery_name", order.name==null?"":order.name);
                    jsonParam.put("delivery_email", order.email==null?"": order.email);
                    jsonParam.put("delivery_mobile", order.mobile==null?"":order.mobile);
                    jsonParam.put("delivery_comment", order.comment==null?"":order.comment);
                    jsonParam.put("total_price", order.totalPrice==null?"":order.totalPrice);
                    jsonParam.put("payment_method", order.methodOfPayment == null ? "" : order.methodOfPayment);

                    // Log.d("Date in json ordercheck", String.valueOf(order.delivery_date));
                    // jsonParam.put("delivery_date", order.delivery_date == null ? "" : order.delivery_date);
                    // Log.d("Date in json check", D_date);
                    // jsonParam.put("delivery_date", D_date==null? "" : D_date );

                    Log.d("delivery date", jsonParam.getString("delivery_date"));
                    Log.d("delivery account", jsonParam.getString("account"));
                    Log.d("delivery dev method", jsonParam.getString("delivery_method"));
                    Log.d("delivery id", jsonParam.getString("customer_id"));
                    Log.d("delivery ward", jsonParam.getString("wardname"));
                    Log.d("delivery name", jsonParam.getString("delivery_name"));
                    Log.d("delivery email", jsonParam.getString("delivery_email"));
                    Log.d("delivery mobile", jsonParam.getString("delivery_mobile"));
                    Log.d("delivery comment", jsonParam.getString("delivery_comment"));
                    Log.d("delivery tprice", jsonParam.getString("total_price"));
                    Log.d("delivery paymethod", jsonParam.getString("payment_method"));





                    JSONArray jarr=new JSONArray();

                    for(int i=0;i<cart.size();i++){
                        Cart crtaItem=cart.get(i);
                        JSONObject jobj=new JSONObject();
                        jobj.put("product_id",crtaItem.productId);
                        // jobj.put("model", crtaItem.model);
                        jobj.put("model",crtaItem.serviceType==null?"":crtaItem.serviceType);
                        jobj.put("name",crtaItem.name==null?"":crtaItem.name);
                        JSONArray jarr1=new JSONArray();
                        for(int j=0;j<crtaItem.options.size();j++){
                            JSONObject jobj1=new JSONObject();
                            Log.d("data ", crtaItem.options.get(j).toString());
                            OptionItem opitem=crtaItem.options.get(j);
                            jobj1.put("product_option_id",opitem.productOptionId);
                            jobj1.put("product_option_value_id",opitem.productOptionValueId);
                            jobj1.put("option_id",opitem.optionId);
                            jobj1.put("option_value_id",opitem.optionValueId);
                            jobj1.put("name",opitem.name==null?"":opitem.name);
                            jobj1.put("value",opitem.value==null?"":opitem.value);
                            jobj1.put("type",opitem.type==null?"":opitem.type);
                            jarr1.put(jobj1);
                        }
                        Log.d("json array", jarr1.toString());
                        jobj.put("option", jarr1);
                        jobj.put("quantity", crtaItem.quantity);
                        jobj.put("price",crtaItem.price);
                        jobj.put("total",crtaItem.total);
                        jobj.put("category",crtaItem.serviceType==null?"":crtaItem.serviceType);

                        Log.d("delivery option", jobj.getString("option"));
                        Log.d("delivery quan", jobj.getString("quantity"));
                        Log.d("delivery price", jobj.getString("price"));
                        Log.d("delivery total", jobj.getString("total"));
                        Log.d("delivery cate", jobj.getString("category"));




                        //jobj.put("delivery_date",crtaItem.delivery_date==null?"":crtaItem.delivery_date);
                        //jobj.put("category",crtaItem.category);
                        //jobj.put("delivery_date",crtaItem.delivery_date==null?"": crtaItem.delivery_date);
                        // jobj.put("delivery_date",order.delivery_date==null?"": order.delivery_date);
                        jarr.put(jobj);
                    }
                    jsonParam.put("cartproducts", jarr);


                    Log.d("cart prod", jarr.toString());

                    Log.d("complete Json", jsonParam.toString());

                    HttpURLConnection urlConnection;
//http://zoukionline.com.au/index.php?route=feed/webapi
                    URL url = new URL("http://alfredocafe.com.au/index.php?route=feed/webapi" + "/orderDetails");
                    //URL url = new URL("http://zoukionline.com.au/index.php?route=feed/webapi" + "/orderDetails");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty("charset", "utf-8");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(jsonParam.toString());
                    writer.flush();
                    int statusCode = urlConnection.getResponseCode();
                    if (statusCode == 200) {
                        BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder response = new StringBuilder();
                        String line;
                        Log.d("before while",r.readLine());
                        while ((line = r.readLine()) != null) {
                            Log.d("line", line);
                            response.append(line);
                        }

                        response_string = response.toString();
                        Log.d("delivery", response_string);

                        // showConfirmationAlert(getString(R.string.success_payment_nav), 0, ManagePaymentFragment.this);
//                    result1 = 1; // Successful
                    } else {
//                    result1 = 0; //"Failed to fetch data!";
                        response_string = null;
                    }

//                    String response_string = Httphelper.call_this_method(jsonParam.toString(), ZoukiApi.SERVER_URL + "/getOrderDetails2", "application/json");
                }catch (Exception e){
                    Log.d("catch exception", e.toString());
                }
                Log.e("ManagePayment", "@@" + response_string);

                return response_string;
            }


        }
        new GetNotificationsDetails().execute();
        hideProgressDialog();

        showToast("Please Check Spam Folder!");

        showConfirmationAlert(getString(R.string.success_payment_nav), 0, ManagePaymentFragment.this);

        //showAlertEditText("Order Placed!", 0, pickupPayment.this);
        List<Cart> items  = zCart.getCartList();
        for (Cart item : items) {
            item.delete();
        }
        zCart.setPickUpInfo(null);
        zCart.setCard(null);


    }


    @Override
    public void onPositiveButtonClick(int alertId) {
        getActivity().finish();
    }

    @Override
    public void onNegativeButtonClick(int alertId) {

    }


}
