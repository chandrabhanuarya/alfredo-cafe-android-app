package wwt.zouki.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import wwt.zouki.BaseFragment;
import wwt.zouki.MainActivity;
import wwt.zouki.R;
import wwt.zouki.ZoukiApplication;
import wwt.zouki.interfaces.AlertIDs;
import wwt.zouki.interfaces.AlertListener;
import wwt.zouki.model.ServiceType;
import wwt.zouki.utils.ZoukiCart;
import wwt.zouki.utils.ZoukiConstants;



public class MenuListFragment extends BaseFragment implements View.OnClickListener, Animation.AnimationListener, AlertListener {
    private Intent nextIntent;
    Animation animFadein, animBounce, animSlideDown, animRotate, animBlink;
    private LinearLayout[] tabList;

    public MenuListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animFadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.fade_in);

        animBounce = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.bounce);

        animRotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate);

        animSlideDown = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_down);

        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink);

        animFadein.setAnimationListener(this);

        View view = getView();


        ZoukiApplication application = (ZoukiApplication) getActivity()
                .getApplication();

     /*   if (!application.isPlacePopupShown()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ZoukiAlertDialog.getInstance().showListDialog(getActivity(),
                            new String[]{"Sunshine Hospital"}, "Select your location", null);
                }
            }, 1000);

            application.setPlacePopupShown(true);
        }
*/

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();


    /*
                view.findViewById(R.id.user);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mActivity != null) {
                    getView().findViewById(R.id.main_frame).setVisibility(View.GONE);
                    getView().findViewById(R.id.footer).setVisibility(View.VISIBLE);
                   mActivity.navigateToFragment(new ManageAccountFragment(), login.class.getName());

                }
            }

        });

*/

        view.findViewById(R.id.user);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mActivity != null) {
                    mActivity.navigateToFragment(new login(), login.class.getName());

                }
            }

        });

        view.findViewById(R.id.contact_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "03-83481803"));
                startActivity(intent);
            }
        });

        view.findViewById(R.id.contact_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");

                emailIntent.putExtra(Intent.EXTRA_EMAIL, "sunshine@zouki.com.au");
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject..");

                startActivity(emailIntent);
            }
        });

        view.findViewById(R.id.privacy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAlertDialog(getString(R.string.privacyPolicy));
            }
        });


      /*  view.findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getView().findViewById(R.id.homebar).setVisibility(View.INVISIBLE);
               // getView().findViewById(R.id.homebar).setVisibility(View.INVISIBLE);
                  if(mActivity!=null){
                      mActivity.navigateToFragment(new MenuListFragment(), MenuListFragment.class.getName());
                  }
            }
        });
        view.findViewById(R.id.cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mActivity!=null){
                    getView().findViewById(R.id.homebar).setVisibility(View.INVISIBLE);
                    mActivity.navigateToFragment(new ManageCartFragment(), ManageCartFragment.class.getName());
                }
            }
        });
        view.findViewById(R.id.user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // getView().findViewById(R.id.toolbar).setVisibility(View.INVISIBLE);
                getView().findViewById(R.id.homebar).setVisibility(View.INVISIBLE);
                if(mActivity!=null){
                    mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                }
            }
        });  */

        view.findViewById(R.id.cafe).setOnClickListener(this);
        view.findViewById(R.id.catering).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        ServiceType existingType = ZoukiCart.getInstance().getServiceTypeInCart();
        ServiceType selectedType = null;

       // nextIntent = new Intent(getActivity(), MainActivity.class);

        //Changes made on 11th July 2016
        nextIntent = new Intent(getActivity(), MainActivity.class);
        int itemId = v.getId();

        switch (itemId) {
            case R.id.cafe:
                if (existingType == null || existingType == ServiceType.CAFE) {
                    selectedType = ServiceType.CAFE;
                } else {
                    String type = ServiceType.CAFE.getTag();

                    showAlertDialog(getString(R.string.diff_item, type.replaceFirst(type.charAt(0) + "",
                            Character.toUpperCase(type.charAt(0)) + "")));

                    return;
                }
                break;

            case R.id.catering:
                if (existingType == null || existingType == ServiceType.CATERING) {
                    selectedType = ServiceType.CATERING;
                } else {
                    String type = ServiceType.CATERING.getTag();

                    showAlertDialog(getString(R.string.diff_item, type.replaceFirst(type.charAt(0) + "",
                            Character.toUpperCase(type.charAt(0)) + "")));

                    return;
                }
                break;


            default:
        }

        if (selectedType != null) {
            ZoukiCart.getInstance().setServiceType(selectedType);
            nextIntent.putExtra(ZoukiConstants.CATEGORY, selectedType.getTag());
            startActivity(nextIntent);
        }
    }

    @Override
    public void onPositiveButtonClick(int alertId) {
        if (alertId == AlertIDs.CONFIRMATION_MIN_QUANTITY) {
            startActivity(nextIntent);
        }
    }

    @Override
    public void onNegativeButtonClick(int alertId) {

    }

    @Override
    public void initLayout() {

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
