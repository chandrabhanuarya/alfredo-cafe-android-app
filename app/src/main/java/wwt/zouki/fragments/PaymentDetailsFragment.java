package wwt.zouki.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;

public class PaymentDetailsFragment extends BaseFragment implements View.OnClickListener {

    public PaymentDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_credit_card, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().findViewById(R.id.smart_card_pay).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new SmartCardFragment(), SmartCardFragment.class.getName());
                }
            }
        });
      /*  getView().findViewById(R.id.smart_cards).setOnClickListener(this);
        getView().findViewById(R.id.create_card).setOnClickListener(this);

        getView().findViewById(R.id.bill_to_company).setOnClickListener(this);
        getView().findViewById(R.id.cost_center).setOnClickListener(this);

        if (ZoukiCart.getInstance().isOnCatering()) {
            getView().findViewById(R.id.cost_center).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.create_card).setVisibility(View.VISIBLE);
        } else {
            getView().findViewById(R.id.smart_cards).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.create_card).setVisibility(View.VISIBLE);
        } */


    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_payment_options);
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;

        switch (v.getId()) {
            case R.id.create_card:
                fragment = new CreditCardFragment();
                break;

            case R.id.bill_to_company:
                fragment = new CompanyAddressFragment();
                break;

            case R.id.smart_cards:
                fragment = new SmartCardFragment();
                break;

            case R.id.cost_center:
                fragment = new CostCenterFragment();
                break;

            default:
        }

        if (mActivity != null) {
            mActivity.navigateToFragment(fragment, fragment.getClass().getName());
        }
    }
}
