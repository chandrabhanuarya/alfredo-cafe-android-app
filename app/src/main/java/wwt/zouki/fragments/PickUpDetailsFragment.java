package wwt.zouki.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.data.PickUpInfo;
import wwt.zouki.utils.ApplicationUtils;
import wwt.zouki.utils.ZoukiCart;

/**
 * Created by Ram Prasad on 10/30/2015.
 */
public class PickUpDetailsFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pickup_details, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().findViewById(R.id.cancel).setOnClickListener(this);
        getView().findViewById(R.id.okay).setOnClickListener(this);


     /*   getView().findViewById(R.id.pickup_option).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {

                    mActivity.navigateToFragment(new DeliveryOptionsFragment(), DeliveryOptionsFragment.class.getName());
                }
            }
        }); */

        getView().findViewById(R.id.delivery_option).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new DeliveryDetailsFragment(), DeliveryDetailsFragment.class.getName());
                }
            }
        });
    }


    @Override
    protected String getTitle() {
        return getString(R.string.title_pickup_details);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.okay:
                PickUpInfo info = new PickUpInfo();
                info.name = ((EditText) getView().findViewById(R.id.name))
                        .getText().toString();

                info.phone = ((EditText) getView().findViewById(R.id.mobile))
                        .getText().toString();

                info.email = ((EditText) getView().findViewById(R.id.email))
                        .getText().toString();

                if (info.name.equals("") && info.phone.equals("") && info.email.equals("")) {

                    showToast("All Fields Are Mandatory.");
                    return;
                } else if (!ApplicationUtils.isEmailValid(info.email)) {
                    showToast("Entered Email is Not Valid.");
                    return;

                } else {
                    ZoukiCart.getInstance().setPickUpInfo(info);
                    //Fragment fragment = new PaymentDetailsFragment();

                    if(ZoukiCart.getInstance().isOnCatering()){
                        Log.d("catering cliked tesing", ZoukiCart.getInstance().getServiceType().toString());
                        Fragment frag = new Delivery_CC();
                        Bundle bundle = new Bundle();
                        bundle.putString("payment_type", "pickup");
                        frag.setArguments(bundle);
                        mActivity.navigateToFragment(frag, frag.getClass().getName());

                    }
                    else{
                        Fragment fragment = new pickupCreditCard();
                        Bundle bundle = new Bundle();
                        bundle.putString("payment_type", "pickup");
                        fragment.setArguments(bundle);
                        mActivity.navigateToFragment(fragment, fragment.getClass().getName());
                    }


                }
                break;

            case R.id.cancel:
                getFragmentManager().popBackStack();
                break;

            default:
        }
    }
}
