package wwt.zouki.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.List;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.adapters.ProductListAdapter;
import wwt.zouki.api.data.Product;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.events.ProductRequestEvent;
import wwt.zouki.utils.ZoukiCart;
import wwt.zouki.utils.ZoukiConstants;

public class  ProductListFragment extends BaseFragment implements Animation.AnimationListener{
    private GridView listView;
    private boolean isSandwich;
    private boolean mGiftsAndFlowers;
    private String mTitle;
    private boolean isBeverages;
    private boolean isFingerFood;
    private TextView ppl;
    private LinearLayout min_ppl;
    Animation animFadein,animBounce, animSlideUp, animRotate, animBlink;

    public ProgressDialog progress;
    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animFadein = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                R.anim.fade_in);

        animBounce = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.bounce);

        animRotate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate);

        animSlideUp = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.slide_up);

        animBlink = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.blink);

        animFadein.setAnimationListener(this);



        Bundle bundle = getArguments();
        if (bundle != null) {
            mTitle = getArguments().getString(ZoukiConstants.TITLE);

            if (bundle.containsKey(ZoukiConstants.TITLE)) {
                isSandwich = bundle.getString(ZoukiConstants.TITLE).equalsIgnoreCase("Sandwich");
            }
            if (bundle.containsKey(ZoukiConstants.TITLE)) {
                isBeverages = bundle.getString(ZoukiConstants.TITLE).equalsIgnoreCase("Beverages");
            }
            if(bundle.containsKey(ZoukiConstants.TITLE)){
                isFingerFood = bundle.getString(ZoukiConstants.TITLE).equalsIgnoreCase("Finger Food");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_list_fragmnet, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchProductList();
    }

    private void fetchProductList() {
        showProgressDialog();


        BusProvider.getInstance().post(new ProductRequestEvent(getArguments()
                .getInt(ZoukiConstants.CATEGORY_ID)));
    }

    @Subscribe
    public void onFeedApiResponse(ApiResponseEvent.Products event) {
        buildProductList(event.getTag());
        hideProgressDialog();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

      //  min_ppl = (LinearLayout) getView().findViewById(R.id.min_ppl);

        listView = (GridView) getView().findViewById(R.id.listView);

        listView.startAnimation(animBounce);
        Bundle bundle = getArguments();
        // changing the bg color of list item
      //  View root = listView.getRootView();
      //  root.setBackgroundColor(getResources().getColor(R.color.item_bg));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Product product = (Product) listView.getItemAtPosition(position);
                Bundle bundle = getArguments();

                bundle.putString(ZoukiConstants.TITLE, product.name);
                bundle.putInt(ZoukiConstants.PRODUCT_ID, product.id);

                String finger_food = bundle.getString(ZoukiConstants.TITLE);
                Log.d("bundle title ", bundle.getString(ZoukiConstants.TITLE));

                if (ZoukiCart.getInstance().isOnCatering()) {
                  //  min_ppl.setVisibility(getView().VISIBLE);
                    bundle.putInt("quantity", product.quantity);
                }

                if(ZoukiCart.getInstance().isOnCafe()){
                    Log.d("product list", String.valueOf(ZoukiCart.getInstance().isOnCafe()));


                }
                if (bundle.getString(ZoukiConstants.TITLE).equalsIgnoreCase("Beverages")) {

                   // product.quantity = Integer.parseInt("1");
                    bundle.putInt("quantity", product.quantity);

                }
                if (bundle.getString(ZoukiConstants.TITLE).equalsIgnoreCase("Sandwich")) {
                    //min_ppl.setVisibility(View.GONE);


                }
                Fragment fragment = new CustomizeItemFragment_backup();
                fragment.setArguments(bundle);

                if (mActivity != null) {
                    mActivity.navigateToFragment(fragment, fragment.getClass().getName());
                }

            }
        });
    }

    private void buildProductList(List <Product> products) {
        ProductListAdapter adapter = (ProductListAdapter) listView.getAdapter();

        if (adapter != null) {
            adapter.updateProductList(products);
        } else {
            adapter = new ProductListAdapter(getActivity(), products, isSandwich, isBeverages, isFingerFood);
            listView.setAdapter(adapter);
        }
    }

    @Override
    protected String getTitle() {
        return mTitle;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
