package wwt.zouki.fragments;

import com.squareup.otto.Subscribe;

import wwt.zouki.api.data.Category;
import wwt.zouki.events.ApiResponseEvent;
import wwt.zouki.events.BusProvider;
import wwt.zouki.events.SubCategoryRequestEvent;
import wwt.zouki.utils.ZoukiConstants;

/**
 * Created by Ram Prasad on 7/7/2015.
 */
public class SubCategoryListFragment extends CategoryListFragment {

    @Override
    protected void fetchCategoryResponse() {
        BusProvider.getInstance().post(new SubCategoryRequestEvent(getArguments()
                .getInt(ZoukiConstants.CATEGORY_ID)));
    }

    @Subscribe
    public void onFeedApiResponse(ApiResponseEvent.Categories event) {
        buildCategoryList(event.getTag());
    }

    @Override
    protected void checkForAlert(Category category) {
        navigateToNextScreen(category);
    }
}
