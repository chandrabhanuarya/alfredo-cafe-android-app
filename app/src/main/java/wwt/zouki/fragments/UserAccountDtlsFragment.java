package wwt.zouki.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.data.User;
import wwt.zouki.utils.ZoukiCart;

public class UserAccountDtlsFragment extends BaseFragment {

    public UserAccountDtlsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_account_dtls, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        User user = ZoukiCart.getInstance().getUserDtls();
        View view = getView();

        if (user != null) {
          //  ((TextView) view.findViewById(R.id.firstName)).setText(user.firstName);
           // ((TextView) view.findViewById(R.id.lastName)).setText(user.lastName);

//            ((TextView) view.findViewById(R.id.phoneNo)).setText(user.phoneNo);
  //          ((TextView) view.findViewById(R.id.email)).setText(user.email);

            view.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    ZoukiCart.getInstance().setUserDtls(null);

                    if (mActivity != null) {
                        mActivity.navigateToFragment(new LoginFragment(), LoginFragment.class.getName());
                    }
                }
            });
        }



    }

    @Override
    protected String getTitle() {
        return getString(R.string.title_account_dtls);
    }
}
