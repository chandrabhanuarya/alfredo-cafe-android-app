package wwt.zouki.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.api.ZoukiService;
import wwt.zouki.interfaces.AlertListener;


public class fragment_forgotpassword extends BaseFragment implements AlertListener {
    public EditText user_email;
    public String email;

    public fragment_forgotpassword() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgotpassword, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View view = getView();


        user_email = (EditText) view.findViewById(R.id.username);

        email = user_email.getText().toString();


        view.findViewById(R.id.login_acc).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                }
            }
        });

        view.findViewById(R.id.forgot).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                if (email != null) {
                    //http://alfredocafe.com.au/index.php?route=feed/webapi/forgotpassword&email=bsindhuja2011@gmail.com
                    ZoukiApi.getService().postForgotPassword(email, new Callback <JsonObject>() {

                        @Override
                        public void success(JsonObject jsonObject, Response response) {

                            Log.d("forgot psd response", jsonObject.toString());
                            JSONObject  jsonRootObject = null;
                            try {
                                jsonRootObject = new JSONObject(response.toString());

                                //Get the instance of JSONArray that contains JSONObjects
                                jsonRootObject.get("success");
                                Log.d("json data", String.valueOf(jsonRootObject.get("success")));
                                showConfirmationAlert("New Password Sent to your Email!", 0, fragment_forgotpassword.this);
// Explicit Intent by specifying its class name
                                Intent i = new Intent(getActivity(), CategoryListFragment.class);
// Starts TargetActivity
                                startActivity(i);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showToast("Please Enter Correct Email!");
                        }
                    });
                } else {
                    showToast("Please Enter Email!");
                    mActivity.navigateToFragment(new fragment_forgotpassword(), fragment_forgotpassword.class.getName());
                }

            }
        });

}

    /*
   private void validateClientDetails() {

        Log.d("Checking client details", String.valueOf(getView().findViewById(R.id.reg_Name)));
        final EditText password = (EditText) getView().findViewById(R.id.reg_password);
        final EditText confirm_psd = (EditText) getView().findViewById(R.id.reg_confirm_psd);
        final EditText name = (EditText) getView().findViewById(R.id.reg_Name);
        final EditText email = (EditText) getView().findViewById(R.id.reg_emailId);
        final EditText phone = (EditText) getView().findViewById(R.id.reg_phoneNo);

        String reg_name = name.getText().toString();
        String reg_psd = password.getText().toString();
        String reg_email = email.getText().toString();
        String reg_phone = phone.getText().toString();
        String reg_confirm_psd = confirm_psd.getText().toString();


        if (!reg_name.equals("") && !reg_email.equals("") && !reg_phone.equals("") && !reg_psd.equals("") && !reg_confirm_psd.equals("")) {

            //showToast("Please enter all the fields!");
            ZoukiApi.getService().postRegistration(String.valueOf(name.getText().toString().trim()), String.valueOf(email.getText().toString().trim()), String.valueOf(password.getText().toString().trim()), String.valueOf(phone.getText().toString().trim()), new Callback<JsonObject>() {

                @Override
                public void success(JsonObject jsonObject, retrofit.client.Response response) {
                    // Log.d("Success", response.toString());
                    //Toast.makeText(n,response,Toast.LENGTH_LONG).show();

                    if (String.valueOf(jsonObject.get("success")) == "true") {

                        // showAlertDialog("Registration Successful!");
                        // showConfirmationAlert("Registration Successful!", 0, fragment_forgotpassword.this);
                        mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                        // Toast.makeText(getActivity(),"Registration Successful!", Toast.LENGTH_SHORT).show();

                    } else {
                        showToast("Registration Unsuccessful!");
                    }
                }

                public void failure(RetrofitError error) {
                    Log.d("TEST", "error : " + error.getMessage());
                    // hideProgressDialog();
                    showAlertDialog("Registration Unsuccessful!");
                    // mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
                }

            });

        } else {
            showToast("All fields are mandatory!");
            mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
        }
    }
*/

    @Override
    protected String getTitle() {
        return getString(R.string.title_forgotpsd);
    }


    @Override
    public void onPositiveButtonClick(int alertId) {

    }

    @Override
    public void onNegativeButtonClick(int alertId) {

    }
}