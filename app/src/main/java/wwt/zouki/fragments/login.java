package wwt.zouki.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import wwt.zouki.api.data.UserLogin;

import com.google.gson.JsonObject;
import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import wwt.zouki.BaseFragment;
import wwt.zouki.R;
import wwt.zouki.api.ZoukiApi;
import wwt.zouki.api.data.SmartCard;
import wwt.zouki.events.BusProvider;
import wwt.zouki.interfaces.AlertListener;


public class login extends BaseFragment implements AlertListener{
    public EditText userEdt, pswdEdt;

    public login() {
        // Required empty public constructor
    }

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Password = "passwordKey";
    public static final String Email = "emailKey";
    SharedPreferences sharedpreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manage_account, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View view = getView();
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        view.findViewById(R.id.login_acc).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                }
            }
        });
        view.findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new fragment_forgotpassword(), fragment_forgotpassword.class.getName());
                }
            }
        });

        view.findViewById(R.id.account_reg).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();

                if (mActivity != null) {
                    mActivity.navigateToFragment(new ClientDetailsFragment(), ClientDetailsFragment.class.getName());
                }
            }
        });

        userEdt = (EditText) view.findViewById(R.id.username);
        pswdEdt = (EditText) view.findViewById(R.id.password);



        view.findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                try {
                    Log.d("logg", String.format("%s%s", userEdt.getText().toString(), pswdEdt.getText().toString()));

                    // new Login().execute();

                    validateLogin();

                } catch (Exception e) {
                    showToast(e.getMessage());
                }
            }
        });
    }

    private void validateLogin() {

        final String username = userEdt.getText().toString();
        final String password = pswdEdt.getText().toString();
        final String[] resp = new String[1];
        final String link = "email"+"="+ username + "&" + "password" + "=" + password;


        // final LinearLayout login_form = (LinearLayout) getView().findViewById(R.id.login_form);

        if (username.trim().length() > 8 && password.trim().length() > 3) {
            Log.d("into login", username);


            class GetNotificationsDetails extends AsyncTask<String, Void, String> {
                String response_string="";
                protected String doInBackground(String... params) {

                    try {


                        HttpURLConnection urlConnection;

                        URL url = new URL(ZoukiApi.SERVER_URL + "/userlogin");
                        urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                        urlConnection.setRequestProperty("charset", "utf-8");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();


                        OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                        writer.write(link);
                        Log.d("writeer", writer.toString() );
                        writer.flush();
                        int statusCode = urlConnection.getResponseCode();
                        if (statusCode == 200) {
                            BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder response = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                response.append(line);
                            }
                            // mActivity.navigateToFragment(new CategoryListFragment(), CategoryListFragment.class.getName());
                            response_string = response.toString();
                            Log.d("response_string", response_string);
                            resp[0] = response_string;
                            if(response_string.equalsIgnoreCase("true")){
                                mActivity.navigateToFragment(new CategoryListFragment(), CategoryListFragment.class.getName());
                                //showConfirmationAlert(getString(R.string.login_success), 0, ManageAccountFragment.this);
                            }
                            else{
                                showToast("Username or Password is Incorrect!");
                                mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
                            }


                        } else {
//                    result1 = 0; //"Failed to fetch data!";
                            response_string = null;
                            showToast("Username or Password is Incorrect!");

                        }

//                    String response_string = Httphelper.call_this_method(jsonParam.toString(), ZoukiApi.SERVER_URL + "/getOrderDetails2", "application/json");
                    }catch (Exception e){
                        Log.d("catch exception", e.toString());
                    }
                    Log.e("ManagePayment", "@@" + response_string);

                    return response_string;
                }

            }
            new GetNotificationsDetails().execute();
            hideProgressDialog();

            // showToast("Please Check Spam Folder!");



        } else {
            showToast("Please Enter Username & Password!");
            mActivity.navigateToFragment(new ManageAccountFragment(), ManageAccountFragment.class.getName());
        }
    }





    @Override
    protected String getTitle() {
        return getString(R.string.title_manage_account);
    }

    @Override
    public void onPositiveButtonClick(int alertId) {

    }

    @Override
    public void onNegativeButtonClick(int alertId) {

    }
}
