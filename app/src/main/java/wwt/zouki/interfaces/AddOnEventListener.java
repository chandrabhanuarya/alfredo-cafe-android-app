package wwt.zouki.interfaces;

import wwt.zouki.model.OptionItem;

/**
 * Created by Ram Prasad on 7/9/2015.
 */
public interface AddOnEventListener {
    void updatePrice(OptionItem option, boolean isChecked);

    void updatePrice(OptionItem option, String cur, String prev);

    void updateComment(OptionItem comment);

    void updateDateTime(int id, OptionItem dateTime);
}
