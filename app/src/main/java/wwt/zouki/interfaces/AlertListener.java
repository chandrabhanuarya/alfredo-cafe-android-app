package wwt.zouki.interfaces;

/**
 * Created by Ram Prasad on 9/3/2015.
 */
public interface AlertListener {
    void onPositiveButtonClick(int alertId);

    void onNegativeButtonClick(int alertId);
}
