package wwt.zouki.interfaces;

/**
 * Created by Ram Prasad on 7/13/2015.
 */
public interface CartEventListener {
    void deleteItem(int position);

    void onCartUpdated();
}
