package wwt.zouki.interfaces;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.view.View;

import wwt.zouki.BaseFragment;

/**
 * Created by Ram Prasad on 7/3/2015.
 */
public interface EventListener {

    void setFragment(BaseFragment fragment);

    void navigateToFragment(Fragment fragment, String tag);

    void navigateToFragment(Fragment fragment, String tag, boolean clearTop);

    void showProgressDialog();

    void hideProgressDialog();

    void showAlert(String message, DialogInterface.OnClickListener listener);

}
