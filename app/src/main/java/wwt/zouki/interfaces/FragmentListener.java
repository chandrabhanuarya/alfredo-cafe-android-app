package wwt.zouki.interfaces;

/**
 * Created by Ram Prasad on 7/3/2015.
 */
public interface FragmentListener {
    boolean isBackPressed();
}
