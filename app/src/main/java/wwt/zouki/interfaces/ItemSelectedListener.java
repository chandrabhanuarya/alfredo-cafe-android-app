package wwt.zouki.interfaces;

/**
 * Created by Ram Prasad on 11/24/2015.
 */
public interface ItemSelectedListener {
    void onItemSelected(Object object);
}
