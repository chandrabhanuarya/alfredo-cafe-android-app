package wwt.zouki.interfaces;

/**
 * Created by Arya on 23/05/2016.
 */
public interface PaymentAlert {
    void onPositiveButtonClick(int alertId);

}
