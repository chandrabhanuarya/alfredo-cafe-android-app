package wwt.zouki.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ram Prasad on 7/9/2015.
 */
@Table(name = "CartItems")
public class Cart extends Model {
    @Column(name = "ServiceType")
    public ServiceType serviceType;

    @Column(name = "Category")
    public String category;

    @SerializedName("delivery_date")
    @Column(name = "DeliveryDate")
    public String delivery_date;

    @Column(name = "Model")
    public String model;

    @Column(name = "Option")
    @SerializedName("option_desc")
    public String optionDesc;

    @SerializedName("product_id")
    @Column(name = "ProductId")
    public int productId;

    @Column(name = "Total")
    public float total;

    @Column(name = "Name")
    public String name;

    @Column(name = "Quantity")
    public int quantity;

    @Column(name = "Price")
    public float price;

    @Column(name = "Type")
    public String type;

    @Column(name = "ImageUrl")
    public String imageURL;

    @SerializedName("option")
    public List<OptionItem> options;

    public Cart() {
        super();
    }

    public void fetchOptions() {
        options = getMany(OptionItem.class, "CartItem");
    }
}
