package wwt.zouki.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ram Prasad on 11/7/2015.
 */
@Table(name = "ItemOptions")
public class OptionItem extends Model {
    @Expose
    @SerializedName("product_option_id")
    @Column(name = "ProductOptionId")
    public int productOptionId;

    @Expose
    @SerializedName("product_option_value_id")
    @Column(name = "ProductOptionValueId")
    public int productOptionValueId;

    @Expose
    @SerializedName("option_id")
    @Column(name = "OptionId")
    public int optionId;

    @Expose
    @SerializedName("option_value_id")
    @Column(name = "OptionValueId")
    public int optionValueId;

    @Expose
    @Column(name = "Name")
    public String name;

    @Expose
    @Column(name = "Value")
    public String value;

    @Expose
    @Column(name = "Type")
    public String type;

    @Expose
    @Column(name = "Price")
    public String price;

    @Column(name = "CartItem", onDelete = Column.ForeignKeyAction.CASCADE)
    public Cart cart;

    public OptionItem() {
        super();
    }
}
