package wwt.zouki.model;

import wwt.zouki.utils.ZoukiConstants;

/**
 * Created by Ram Prasad on 10/24/2015.
 */
public enum ServiceType {
    CAFE, CATERING, CAKES, FLOWERS;

    public int getValue() {
        switch (this) {
            case CAFE:
                return 1;
            case CATERING:
                return 2;
            case CAKES:
                return 3;
            case FLOWERS:
                return 4;

            default:
                return 0;
        }
    }

    public String getTag() {
        switch (this) {
            case CAFE:
                return ZoukiConstants.CATEGORY_CAFE;
            case CATERING:
                return ZoukiConstants.CATEGORY_CATERING;
            case CAKES:
                return ZoukiConstants.CATEGORY_CAKES;
            case FLOWERS:
                return ZoukiConstants.CATEGORY_FLOWERS;

            default:
                return "";
        }
    }
}
