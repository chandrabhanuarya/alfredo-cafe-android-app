package wwt.zouki.model;

/**
 * Created by Ram Prasad on 8/20/2015.
 */
public class ValidationException extends RuntimeException {
    private static final long serialVersionUID = -743862426460328113L;

    public ValidationException() {
        super("Validation Failed");
    }

    public ValidationException(Throwable throwable) {
        super(throwable);
    }

    public ValidationException(String msg) {
        super(msg);
    }

    public ValidationException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

}
