package wwt.zouki.utils;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created by Ram Prasad on 9/10/2015.
 */
public class ApplicationUtils {

    public static String getWellFormedURL(String imageURL) {
        return imageURL.replaceAll(" ", "%20").replace("https", "http");
    }

    public static String roundOffValue(float value) {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        return df.format(value);
    }

    public static boolean isEmailValid(String email) {
        return Pattern.compile(ZoukiConstants.EMAIL_PATTERN).matcher(email).matches();
    }
}
