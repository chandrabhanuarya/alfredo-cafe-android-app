package wwt.zouki.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * @author Ram Prasad
 */
public class FontUtils {


    /**
     * OpenSans Font Style Bold.
     */
    private static final String FUTURA_BOLD = "Futura-Bold.ttf";

    /**
     * OpenSans Font Style Regualar.
     */
    private static final String FUTURA_REGULAR = "Futura_Medium_BT.ttf";

    /**
     * The Typeface specifies the openSans Bold typeface and intrinsic style of a font.
     */
    private static Typeface mTypefaceFuturaBold = null;

    /**
     * The Typeface specifies the opensans regualar typeface and intrinsic style of a font.
     */
    private static Typeface mTypefaceFuturaRegular = null;

    /**
     * To Get OpenSans font for Text View.
     *
     * @return Typeface
     */
    public static Typeface getFuturaTypeface(Context context, int style) {
        if (style == Typeface.BOLD) {
            if (mTypefaceFuturaBold == null) {
                mTypefaceFuturaBold = Typeface.createFromAsset(context.getAssets(), FUTURA_BOLD);
            }

            return mTypefaceFuturaBold;
        } else {
            if (mTypefaceFuturaRegular == null) {
                mTypefaceFuturaRegular = (Typeface
                        .createFromAsset(context.getAssets(), FUTURA_REGULAR));
            }

            return mTypefaceFuturaRegular;
        }
    }
}
