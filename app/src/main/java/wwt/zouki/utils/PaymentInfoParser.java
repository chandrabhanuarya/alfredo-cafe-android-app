package wwt.zouki.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import wwt.zouki.model.PaymentInfo;

/**
 * Created by Ram Prasad on 8/18/2015.
 */
public class PaymentInfoParser extends DefaultHandler {
    private PaymentInfo paymentInfo;
    private String buffer;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("SecurePayMessage")) {
            paymentInfo = new PaymentInfo();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        buffer = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("statusCode")) {
            paymentInfo.setStatus(buffer);
        } else if (qName.equals("amount")) {
            paymentInfo.setAmount(buffer);
        } else if (qName.equals("purchaseOrderNo")) {
            paymentInfo.setPurchaseOrderNo(buffer);
        } else if (qName.equals("responseCode")) {
            paymentInfo.setResponseCode(buffer);
        } else if (qName.equals("approved")) {
            paymentInfo.setApproved(buffer.equals("Yes"));
        } else if(qName.equals("statusDescription")) {
            paymentInfo.setStatusDescription(buffer);
        }else if(qName.equals("responseText")){
            paymentInfo.setResponseText(buffer);
        }
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

}
