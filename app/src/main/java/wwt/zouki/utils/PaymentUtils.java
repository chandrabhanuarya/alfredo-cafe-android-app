package wwt.zouki.utils;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.UUID;

import wwt.zouki.model.Card;

/**
 * Created by Ram Prasad on 8/15/2015.
 */
public class PaymentUtils {
    private static final String NAME_SPACE = "";

    private XmlSerializer serializer;
    private StringWriter writer;
    private int totalCost;

    public String getXML() throws IOException {
        serializer = Xml.newSerializer();
        writer = new StringWriter();

        serializer.setOutput(writer);
        serializer.startDocument("UTF-8", true);

        buildSecurePayInfo();

        serializer.endDocument();
        Log.d("StringWriter****", writer.toString());
        return writer.toString();
    }

    private void buildSecurePayInfo() throws IOException {
        serializer.startTag(NAME_SPACE, "SecurePayMessage");
        buildMessageInfo();
        buildMerchantInfo();

        serializer.startTag(NAME_SPACE, "RequestType");
        serializer.text("Payment");
        serializer.endTag(NAME_SPACE, "RequestType");

        buildPaymentInfo();
        serializer.endTag(NAME_SPACE, "SecurePayMessage");
    }

    private void buildMessageInfo() throws IOException {
        serializer.startTag(NAME_SPACE, "MessageInfo");

        //message has to be changed dynamically generated
        serializer.startTag(NAME_SPACE, "messageID");
        String randomNumber = randomStringOfLength(30);
        serializer.text(randomNumber);

        serializer.endTag(NAME_SPACE, "messageID");

        serializer.startTag(NAME_SPACE, "messageTimestamp");
        serializer.text(new Date().getTime() + "+660");
        serializer.endTag(NAME_SPACE, "messageTimestamp");

        serializer.startTag(NAME_SPACE, "timeoutValue");
        serializer.text("60");
        serializer.endTag(NAME_SPACE, "timeoutValue");

        serializer.startTag(NAME_SPACE, "apiVersion");
        serializer.text("xml-4.2");
        serializer.endTag(NAME_SPACE, "apiVersion");

        serializer.endTag(NAME_SPACE, "MessageInfo");
    }

    private String randomStringOfLength(int length) {
        StringBuffer buffer = new StringBuffer();
        while (buffer.length() < length) {
            buffer.append(uuidString());
        }
        return buffer.substring(0, length);
    }
    private static String uuidString() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private void buildMerchantInfo() throws IOException {
        serializer.startTag(NAME_SPACE, "MerchantInfo");

        serializer.startTag(NAME_SPACE, "merchantID");
        serializer.text(ZoukiConstants.MERCHANT_ID);
        serializer.endTag(NAME_SPACE, "merchantID");

        serializer.startTag(NAME_SPACE, "password");
        serializer.text(ZoukiConstants.MRCHNT_PSWD);
        serializer.endTag(NAME_SPACE, "password");

        serializer.endTag(NAME_SPACE, "MerchantInfo");
    }

    private void buildPaymentInfo() throws IOException {
        serializer.startTag(NAME_SPACE, "Payment");

        serializer.startTag(NAME_SPACE, "TxnList");
        serializer.attribute(NAME_SPACE, "count", "1");

        serializer.startTag(NAME_SPACE, "Txn");
        serializer.attribute(NAME_SPACE, "ID", "1");

        serializer.startTag(NAME_SPACE, "txnType");
        serializer.text("10");
        serializer.endTag(NAME_SPACE, "txnType");

        serializer.startTag(NAME_SPACE, "txnSource");
        serializer.text("23");
        serializer.endTag(NAME_SPACE, "txnSource");

        serializer.startTag(NAME_SPACE, "amount");
        String totalCost = ZoukiCart.getInstance().getTotalCost().replace("$", "");
        if(totalCost.contains(".")) {
            String[] cost = totalCost.split("\\.");
            if(cost.length == 2){
                totalCost = cost[0];
            }
        }
        this.totalCost = Integer.parseInt(totalCost)*100;
        //Log.d("TotalCost", String.valueOf(this.totalCost));
        serializer.text(String.valueOf(this.totalCost));
        serializer.endTag(NAME_SPACE, "amount");

        serializer.startTag(NAME_SPACE, "currency");
        serializer.text("AUD");
        serializer.endTag(NAME_SPACE, "currency");

        serializer.startTag(NAME_SPACE, "purchaseOrderNo");
        serializer.text("orderNumber_" +randomStringOfLength(30));
        serializer.endTag(NAME_SPACE, "purchaseOrderNo");

        buildCreditCardInfo();
        buildBuyerInfo();

        serializer.endTag(NAME_SPACE, "Txn");
        serializer.endTag(NAME_SPACE, "TxnList");

        serializer.endTag(NAME_SPACE, "Payment");
    }


    private void buildCreditCardInfo() throws IOException {
        Card card = ZoukiCart.getInstance().getCard();
        serializer.startTag(NAME_SPACE, "CreditCardInfo");

        serializer.startTag(NAME_SPACE, "cardNumber");
        serializer.text(card.getCardNumber());
        serializer.endTag(NAME_SPACE, "cardNumber");

        serializer.startTag(NAME_SPACE, "expiryDate");
        serializer.text(card.getExpiryDate());
        serializer.endTag(NAME_SPACE, "expiryDate");

        serializer.startTag(NAME_SPACE, "cvv");
        serializer.text(card.getCvv());
        serializer.endTag(NAME_SPACE,"cvv");

        serializer.endTag(NAME_SPACE, "CreditCardInfo");
    }

    private void buildBuyerInfo() throws IOException {
        Card card = ZoukiCart.getInstance().getCard();
        serializer.startTag(NAME_SPACE, "BuyerInfo");
        serializer.startTag(NAME_SPACE, "firstName");
        serializer.text(card.getCardName());
        serializer.endTag(NAME_SPACE, "firstName");
        serializer.startTag(NAME_SPACE, "lastName");
        serializer.text("");
        serializer.endTag(NAME_SPACE, "lastName");
        serializer.endTag(NAME_SPACE, "BuyerInfo");
    }

}
