package wwt.zouki.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by Ram Prasad on 10/8/2015.
 */
public class PreferenceManager {
    String PREF_NAME = "ZOUKI";
    int PRIVATE_MODE = 0;
    Context mContext;

    public PreferenceManager(Context context) {
        this.mContext = context;
    }

    private SharedPreferences getStringPref() {
        return mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    /**
     * Method to set string preference.
     *
     * @param key   key for preference
     * @param value value to store in preference
     */
    public void setStringPref(String key, String value) {
        Editor editor = getStringPref().edit();

        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Method to return a string preference based on supplied key.
     *
     * @param key key to fetch preference
     * @return string preference value
     */
    public String getStringPref(String key) {
        return getStringPref().getString(key, "");
    }

    /**
     * Method to set boolean preference.
     *
     * @param key   key for preference
     * @param value value to store in preference
     */
    public void setBooleanPref(String key, boolean value) {
        Editor editor = getStringPref().edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Method to return a boolean preference based on supplied key.
     *
     * @param key key to fetch preference
     * @return boolean preference value
     */
    public boolean getBooleanPref(String key) {
        return getStringPref().getBoolean(key, false);
    }

    /**
     * Clearing existing preferences.
     */
    public void clearPreferences() {
        Editor editor = getStringPref().edit();
        editor.clear();

        editor.commit();
    }
}
