package wwt.zouki.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

import wwt.zouki.interfaces.ItemSelectedListener;

public class ZoukiAlertDialog {
    private static ZoukiAlertDialog mEasyCommuteDialog = null;
    private AlertDialog mAlertDialog = null;

    public static ZoukiAlertDialog getInstance() {
        if (mEasyCommuteDialog == null) {
            mEasyCommuteDialog = new ZoukiAlertDialog();
        }
        return mEasyCommuteDialog;
    }

    private ZoukiAlertDialog() {
        // no instances
    }

    public void showAlertDialog(Context context, String message) {
        try {
            if (message != null && message.length() > 0 && (mAlertDialog == null
                    || !mAlertDialog.isShowing())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                TextView titleText = new TextView(context);

                titleText.setTextColor(Color.WHITE);
                titleText.setTextSize(20);
                titleText.setPadding(10, 10, 10, 10);
                titleText.setBackgroundColor(Color.DKGRAY);
                titleText.setGravity(Gravity.CENTER);

                builder.setCustomTitle(titleText);
                builder.setMessage(message);
                builder.setCancelable(false);

                builder.setPositiveButton("OK", null);
                mAlertDialog = builder.show();

                TextView messageText = (TextView) mAlertDialog.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);
            }
        } catch (Exception ignored) {
        }
    }

    public void showListDialog(Context context, final String[] array, String title,
                               final ItemSelectedListener callBack) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setItems(array, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int position) {
                            if (array != null && callBack != null) {
                                callBack.onItemSelected(array[position]);
                            }
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        } catch (Exception ignored) {
        }
    }
}