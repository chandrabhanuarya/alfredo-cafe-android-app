package wwt.zouki.utils;

import com.activeandroid.query.Select;

import java.util.List;

import wwt.zouki.ZoukiApplication;
import wwt.zouki.api.data.PickUpInfo;
import wwt.zouki.api.data.User;
import wwt.zouki.model.Card;
import wwt.zouki.model.Cart;
import wwt.zouki.model.DeliveryDetails;
import wwt.zouki.model.ServiceType;

/**
 * Created by Ram Prasad on 7/9/2015.
 */
public class ZoukiCart {
    private static ZoukiCart zoukiCart = new ZoukiCart();
    private ZoukiApplication application;
    private boolean isOnCatering;

    private DeliveryDetails deliveryDtls;
    private PickUpInfo pickUpInfo;
    private String isDelivery;
    public String pryAddress;
    private User userDtls;
    private Card card;

    private String deliveryDate;
    private String deliveryTime;

    private ServiceType serviceType;

    public static ZoukiCart getInstance() {
        return zoukiCart;
    }

    public ZoukiCart() {
    }

    public List<Cart> getCartList() {
        return new Select().all().from(Cart.class).execute();
    }

    public List<Cart> getCartList(ServiceType serviceType) {
        return new Select().all().from(Cart.class).where("ServiceType = ?", serviceType).execute();
    }

    public ServiceType getServiceType() {
        return this.serviceType;
    }

    public ServiceType getServiceTypeInCart() {
        List<Cart> cart = getCartList();

        if (cart.size() > 0) {
            return cart.get(0).serviceType;
        }

        return null;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getTotalCost() {
        List<Cart> cartList = getCartList();
        float total = 0;

        for (Cart cart : cartList) {
            total = total + cart.total;
        }

        return "$" + ApplicationUtils.roundOffValue(total);
    }

    public ZoukiApplication getApplication() {
        return application;
    }

    public void setApplication(ZoukiApplication application) {
        this.application = application;
    }

    public User getUserDtls() {
        return userDtls;
    }

    public void setUserDtls(User userDtls) {
        this.userDtls = userDtls;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public boolean isOnCatering() {
        return serviceType == ServiceType.CATERING;
    }
    public  boolean isOnCafe(){
        return serviceType == ServiceType.CAFE;
    }

    public PickUpInfo getPickUpInfo() {
        return pickUpInfo;
    }

    public void setPickUpInfo(PickUpInfo pickUpInfo) {
        this.pickUpInfo = pickUpInfo;
    }

    public DeliveryDetails getDeliveryDtls() {
        return deliveryDtls;
    }

    public void setDeliveryDtls(DeliveryDetails deliveryDtls) {
        this.deliveryDtls = deliveryDtls;
    }

    public String isDelivery() {
        return isDelivery;
    }

    public String setDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
        return isDelivery;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
}
