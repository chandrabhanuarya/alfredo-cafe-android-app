package wwt.zouki.utils;

/**
 * Created by Ram Prasad on 7/6/2015.
 */
public class ZoukiConstants {

    // CATEGORY CONSTANTS
    public static final String CATEGORY_CAFE = "cafe";
    public static final String CATEGORY_CATERING = "catering";
    public static final String CATEGORY_CAKES = "cakes";
    public static final String CATEGORY_FLOWERS = "flowers";

    // KEY CONSTANTS
    public static final String CATEGORY = "category";
    public static final String CATEGORY_ID = "categoryId";
    public static final String PRODUCT_ID = "productId";

    public static final String TITLE = "title";

    // MERCHANT DETAILS
    public static final String MERCHANT_ID = "ABC0001";
    public static final String MRCHNT_PSWD = "abc123";

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

}
