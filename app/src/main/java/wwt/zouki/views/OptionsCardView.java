package wwt.zouki.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import wwt.zouki.R;
import wwt.zouki.api.data.Option;
import wwt.zouki.api.data.OptionValue;
import wwt.zouki.interfaces.AddOnEventListener;
import wwt.zouki.model.OptionItem;

/**
 * Created by Ram Prasad on 9/5/2015.
 */
public class OptionsCardView extends LinearLayout {
    protected static final int TIME_DIALOG_ID = 999;
    protected static final int DATE_DIALOG_ID = 998;

    private List<OptionValue> optionValues;
    private AddOnEventListener mListener;

    private boolean isOptionSelected;
    private String prevSelection;
    private boolean isRequired;

    public OptionsCardView(Context context) {
        super(context);
    }

    public OptionsCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OptionsCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void buildCardView(Option option, AddOnEventListener listener) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        optionValues = option.optionValues;
        isRequired = option.required == 1;

        String optionType = option.type;
        this.mListener = listener;
        View view = null;

        if (optionType.equals("radio") || optionType.equals("select")) {
            view = inflater.inflate(R.layout.item_radio_group, null);

            OptionItem item = new OptionItem();
            item.optionId = option.optionId;
            item.name = option.name;
            item.type = option.type;

            RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
            radioGroup.setOnCheckedChangeListener(new SingleSelectionListener(item));

            for (OptionValue optionValue : optionValues) {
                RadioButton radioBtn = (RadioButton) inflater.inflate(R.layout
                        .item_option_radiobtn, null);

                radioBtn.setId(optionValue.optionValueId);
                radioBtn.setText(optionValue.name);
                radioGroup.addView(radioBtn);
            }
        } else if (optionType.equals("checkbox")) {
            view = inflater.inflate(R.layout.item_check_group, null);
            LinearLayout checkLayout = (LinearLayout) view.findViewById(R.id.check_group);

            for (OptionValue optionValue : optionValues) {
                CheckBox checkBox = (CheckBox) inflater.inflate(R.layout
                        .item_option_checkbox, null);

                OptionItem item = new OptionItem();
                item.optionId = option.optionId;
                item.name = option.name;
                item.type = option.type;

                item.productOptionValueId = optionValue.productOptionValueId;
                item.optionValueId = optionValue.optionValueId;

                item.price = optionValue.price;
                item.value = optionValue.name;

                checkBox.setOnCheckedChangeListener(new MultiSelectionListener(item));
                checkBox.setText(optionValue.name);

                checkLayout.addView(checkBox);
            }
        } else if (optionType.equals("textarea")) {
            view = inflater.inflate(R.layout.item_option_textarea, null);
            final EditText comment = (EditText) view.findViewById(R.id.textArea);

            final OptionItem item = new OptionItem();
            item.optionId = option.optionId;
            item.name = option.name;
            item.type = option.type;

            comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        if (mListener != null) {
                            item.value = comment.getText().toString();
                            mListener.updateComment(item);
                        }

                        return true;
                    } else {
                        return false;
                    }
                }
            });

            ((TextView) view.findViewById(R.id.optionLabel)).setText(
                    option.name);
        } else if (optionType.equals("date") || optionType.equals("time")) {
            view = inflater.inflate(R.layout.item_option_date, null);

            TextView tv = (TextView) view.findViewById(R.id.title);
            tv.setText(option.name);

            final TextView dateView = (TextView) view.findViewById(R.id.date_field);
            final int id = optionType.equals("date") ? DATE_DIALOG_ID : TIME_DIALOG_ID;

            final OptionItem item = new OptionItem();
            item.optionId = option.optionId;
            item.name = option.name;
            item.type = option.type;

            view.findViewById(R.id.date_picker).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    createDialog(id, dateView, item).show();
                }
            });
        }

        //Log.d("Checking.. Option Type", optionType);

        if (optionType.equals("radio") || optionType.equals("checkbox")) {
             ((TextView) view.findViewById(R.id.sectionTitle)).setText((isRequired
                    ? "* " : "") + option.name);
        } else {
            TextView sec_title = (TextView) view.findViewById(R.id.sectionTitle);
            sec_title.setText("");
           // Log.d("App Crashing....", optionType);

        }

        if(view != null){
            addView(view);
        }

    }

    public Dialog createDialog(int id, TextView dateTimeView, OptionItem option) {
        Calendar cal = Calendar.getInstance();

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(getContext(), new OptionDatePicker(dateTimeView, option), cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

            case TIME_DIALOG_ID:
                return new TimePickerDialog(getContext(), new OptionTimePicker(dateTimeView, option),
                        cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);

            default:
                return null;
        }
    }

    private class OptionDatePicker implements DatePickerDialog.OnDateSetListener {
        private TextView datePicker;
        private OptionItem option;

        public OptionDatePicker(TextView datePicker, OptionItem option) {
            this.datePicker = datePicker;
            this.option = option;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, monthOfYear, dayOfMonth);

            if (cal.getTime().before(new Date())) {
                Toast.makeText(getContext(), "You have selected past date",
                        Toast.LENGTH_SHORT).show();
            } else {
                datePicker.setText(new StringBuilder().append(pad(dayOfMonth))
                        .append("/").append(pad(monthOfYear)).append("/").append(pad(year)));

                option.value = datePicker.getText().toString();
                mListener.updateDateTime(DATE_DIALOG_ID, option);
            }
        }
    }

    private class OptionTimePicker implements TimePickerDialog.OnTimeSetListener {
        private TextView timePicker;
        private OptionItem option;

        public OptionTimePicker(TextView timePicker, OptionItem option) {
            this.timePicker = timePicker;
            this.option = option;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar cal = Calendar.getInstance();

            cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cal.set(Calendar.MINUTE, minute);

            if (cal.getTime().before(new Date())) {
                Toast.makeText(getContext(), "You have selected past time",
                        Toast.LENGTH_SHORT).show();
            } else {
                timePicker.setText(new StringBuilder().append(pad(hourOfDay > 12 ? hourOfDay - 12 : hourOfDay))
                        .append(":").append(pad(minute)) + " " + (hourOfDay > 12 ? "PM" : "AM"));

                option.value = timePicker.getText().toString();
                mListener.updateDateTime(TIME_DIALOG_ID, option);
            }
        }
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private class SingleSelectionListener implements RadioGroup.OnCheckedChangeListener {
        private OptionItem item;

        public SingleSelectionListener(OptionItem item) {
            this.item = item;
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (mListener != null) {
                OptionValue option = getSelectedItem(checkedId);
                isOptionSelected = true;

                item.productOptionValueId = option.productOptionValueId;
                item.optionValueId = option.optionValueId;

                item.price = option.price;
                item.value = option.name;

                mListener.updatePrice(item, option.price, prevSelection);
                prevSelection = option.price;
            }
        }
    }

    private class MultiSelectionListener implements OnCheckedChangeListener {
        private OptionItem item;

        public MultiSelectionListener(OptionItem item) {
            this.item = item;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (mListener != null) {
                mListener.updatePrice(item, isChecked);
                isOptionSelected = true;
            }
        }
    }

    private OptionValue getSelectedItem(int optionId) {
        for (OptionValue value : optionValues) {
            if (value.optionValueId == optionId) {
                return value;
            }
        }

        return null;
    }

    public boolean isOptionSelectionMissed() {
        return !isOptionSelected && isRequired;
    }
}
