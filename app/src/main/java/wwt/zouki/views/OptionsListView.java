package wwt.zouki.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

/**
 * Created by Ram Prasad on 9/5/2015.
 */
public class OptionsListView extends LinearLayout {
    private BaseAdapter adapter;

    public OptionsListView(Context context) {
        super(context);
    }

    public OptionsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OptionsListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
        notifyListItems();
    }

    public BaseAdapter getAdapter() {
        return this.adapter;
    }

    public Object getItemAtPosition(int position) {
        return this.adapter.getItem(position);
    }

    public void notifyListItems() {
        int size = this.adapter.getCount();

        for (int i = 0; i < size; i++) {
            View item = this.adapter.getView(i, null, this);

            if (item != null) {
                addView(item);
            }
        }
    }

    public boolean isMandatoryItemsMissed() {
        int size = getChildCount();

        for (int i=0; i<size; i++) {
            OptionsCardView cardView = (OptionsCardView) getChildAt(i);

            if (cardView.isOptionSelectionMissed()) {
                return true;
            }
        }

        return false;
    }
}
