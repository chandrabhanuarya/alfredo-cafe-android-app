package wwt.zouki.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import wwt.zouki.R;
import wwt.zouki.adapters.DeliveryItemListAdapter;
import wwt.zouki.model.Cart;

/**
 * Created by Ram Prasad on 10/25/2015.
 */
public class ServiceItemView extends LinearLayout {
    protected static final int TIME_DIALOG_ID = 999;
    protected static final int DATE_DIALOG_ID = 998;

    private int day, month, year;
    private int hour, minute;

    private TextView timePicker;
    private TextView datePicker;

    public ServiceItemView(Context context) {
        super(context);
    }

    public ServiceItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServiceItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void buildServiceItemList(List<Cart> cart) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.item_cart_service, null);

        ServiceListView listView = (ServiceListView) view.findViewById(R.id.serviceItem);
        listView.setAdapter(new DeliveryItemListAdapter(getContext(), cart, null));

        // Render View
        Calendar cal = Calendar.getInstance();

        timePicker = (TextView) view.findViewById(R.id.timePicker);
        datePicker = (TextView) view.findViewById(R.id.datePicker);

        datePicker.setText(cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1)
                + "/" + cal.get(Calendar.YEAR));

        timePicker.setText(cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
                + " " + (cal.get(Calendar.AM_PM) == 0 ? "AM" : "PM"));

        view.findViewById(R.id.timePicker).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createDialog(TIME_DIALOG_ID).show();
            }
        });

        view.findViewById(R.id.datePicker).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                createDialog(DATE_DIALOG_ID).show();
            }
        });

        addView(view);
    }

    public Dialog createDialog(int id) {
        Calendar cal = Calendar.getInstance();

        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);

        hour = cal.get(Calendar.HOUR_OF_DAY);
        minute = cal.get(Calendar.MINUTE);

        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(getContext(), timePickerListener, hour, minute, true);

            case DATE_DIALOG_ID:
                // set time picker as current time
                return new DatePickerDialog(getContext(), datePickerListener, year, month, day);
        }

        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    Calendar cal = Calendar.getInstance();

                    cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                    cal.set(Calendar.MINUTE, selectedMinute);

                    if (cal.getTime().before(new Date())) {
                        showToast("You have selected past time");
                    } else {
                        hour = selectedHour;
                        minute = selectedMinute;

                        // set current time
                        timePicker.setText(new StringBuilder().append(pad(hour > 12 ? hour - 12 : hour))
                                .append(":").append(pad(minute)) + " " + (hour > 12 ? "PM" : "AM"));
                    }

                }
            };

    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int years, int monthOfYear,
                                      int dayOfMonth) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(years, monthOfYear, dayOfMonth);

                    if (cal.getTime().before(new Date())) {
                        showToast("You have selected past date");
                    } else {
                        day = dayOfMonth;
                        month = monthOfYear;
                        year = years;

                        // set current date
                        datePicker.setText(new StringBuilder().append(pad(day))
                                .append("/").append(pad(month)).append("/").append(pad(year)));
                    }
                }
            };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
