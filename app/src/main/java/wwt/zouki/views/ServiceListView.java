package wwt.zouki.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

/**
 * Created by Ram Prasad on 10/24/2015.
 */
public class ServiceListView extends LinearLayout {
    private BaseAdapter adapter;

    public ServiceListView(Context context) {
        super(context);
    }

    public ServiceListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServiceListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {

    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
        notifyListItems();
    }

    public BaseAdapter getAdapter() {
        return this.adapter;
    }

    public Object getItemAtPosition(int position) {
        return this.adapter.getItem(position);
    }

    public void notifyListItems() {
        int size = this.adapter.getCount();

        for (int i = 0; i < size; i++) {
            View item = this.adapter.getView(i, null, this);

            if (item != null) {
                addView(item);
            }
        }
    }

}
