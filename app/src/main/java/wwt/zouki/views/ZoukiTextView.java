package wwt.zouki.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ram Prasad.
 */
public class ZoukiTextView extends TextView {
    public ZoukiTextView(Context context) {
        super(context);
    }

    public ZoukiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ZoukiTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the typeface and style in which the text should be displayed,
     * and turns on the fake bold and italic bits in the Paint if the Typeface that
     * you provided does not have all the bits in the style that you specified.
     *
     * @param tf and style
     */
//    @Override
//    public void setTypeface(Typeface tf, int style) {
//        super.setTypeface(FontUtils.getFuturaTypeface(getContext(), style));
//    }
}
